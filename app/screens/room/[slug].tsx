/**
 * File: /screens/room/[slug].tsx
 * Project: app
 * File Created: 06-01-2025 15:49:21
 * Author: ffx
 * -----
 * BitSpur (c) Copyright 2021 - 2025
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { withAuthenticated } from "@multiplatform.one/keycloak";
import { GeneralQuestion } from "app/components/GeneralQuestions/index.js";
import { QuestionsCard } from "app/components/QuestionaCard/index.js";
import { useState } from "react";
import { YStack } from "ui";

const RoomScreen = () => {
  const [show, setShow] = useState(false);
  return (
    <YStack fullscreen padding="$10" ai="center" jc="center">
      {!show && <GeneralQuestion setShot={setShow} />}
      {show && <QuestionsCard />}
    </YStack>
  );
};

export const Screen = withAuthenticated(RoomScreen);
