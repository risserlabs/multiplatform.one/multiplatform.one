/**
 * File: /screens/join-room/index.tsx
 * Project: app
 * File Created: 04-01-2025 09:50:43
 * Author: ffx
 * -----
 * BitSpur (c) Copyright 2021 - 2025
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { withAuthenticated } from "@multiplatform.one/keycloak";
import { JoinRoomCard } from "app/components/JoinRoomCard/index.js";
import { YStack } from "ui";

const JoinRoomScreen = () => {
  return (
    <YStack fullscreen ai="center" jc="center">
      <JoinRoomCard />
    </YStack>
  );
};

export const Screen = withAuthenticated(JoinRoomScreen);
