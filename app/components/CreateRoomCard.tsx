import { ArrowLeft, Users } from "@tamagui/lucide-icons";
import type { RoomDataType } from "app/screens/create-room";
import { useRouter } from "one";
import { useState } from "react";
/**
 * File: /components/CreateRoomCard.tsx
 * Project: app
 * File Created: 04-01-2025 15:04:07
 * Author: ffx
 * -----
 * BitSpur (c) Copyright 2021 - 2025
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Button, Card, H4, Input, Paragraph, Text, XStack, YStack } from "ui";
import { Dialog, Label } from "ui"; // Added missing imports

export const CreateRoomCard = ({
  setRoomData,
}: {
  setRoomData: (data: RoomDataType) => void;
}) => {
  const [formData, setFormData] = useState({
    roomName: "",
  });

  const router = useRouter();

  const generateRoomCode = () => {
    const upperCase = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    const lowerCase = "abcdefghijklmnopqrstuvwxyz";
    const numbers = "0123456789";
    const specialChars = "!@#$%^&*";

    let code = "";
    // Ensure at least one of each type
    code += upperCase[Math.floor(Math.random() * upperCase.length)];
    code += lowerCase[Math.floor(Math.random() * lowerCase.length)];
    code += numbers[Math.floor(Math.random() * numbers.length)];
    code += specialChars[Math.floor(Math.random() * specialChars.length)];

    // Fill remaining 2 characters randomly
    const allChars = upperCase + lowerCase + numbers + specialChars;
    for (let i = 0; i < 2; i++) {
      code += allChars[Math.floor(Math.random() * allChars.length)];
    }

    // Shuffle the code
    return code
      .split("")
      .sort(() => Math.random() - 0.5)
      .join("");
  };

  const handleSubmit = () => {
    if (!formData.roomName) {
      return;
    }

    const roomCode = generateRoomCode();
    const inviteLink = `https://game.example.com/join/${roomCode}`;
    const submissionData: RoomDataType = {
      roomName: formData.roomName,
      roomCode: roomCode,
      inviteLink: inviteLink,
    };

    setRoomData(submissionData);

    console.log("Form submission data:", submissionData);
  };

  return (
    <YStack ai="center" jc="center" paddingTop="$10">
      <Card
        backgroundColor="$color1"
        borderRadius="$3"
        width="100%"
        maxWidth={500}
        minWidth={300}
        padding="$4"
        gap="$4"
      >
        <YStack>
          <XStack gap="$4">
            <Button
              onPress={() => router.push("/")}
              backgroundColor="$backgroundTransparent"
              hoverStyle={{
                backgroundColor: "$backgroundTransparent",
              }}
            >
              <ArrowLeft />
            </Button>
            <YStack>
              <H4>Create Room</H4>
              <Paragraph>Set up your game room</Paragraph>
            </YStack>
          </XStack>
        </YStack>

        <YStack gap="$4">
          <YStack gap="$4">
            <Text>Room Name</Text>
            <Input
              placeholder="Room Name"
              value={formData.roomName}
              onChangeText={(text) =>
                setFormData((prev) => ({ ...prev, roomName: text }))
              }
            />
          </YStack>
        </YStack>

        <YStack paddingTop="$4">
          <XStack>
            <Button
              onPress={handleSubmit}
              hoverStyle={{
                backgroundColor: "$color10",
              }}
              color="$color12"
              width="100%"
              borderColor="$color12"
              borderRadius="$3"
              backgroundColor="$color1"
              padding="$4"
              fontWeight="bold"
              disabled={!formData.roomName}
            >
              <Users />
              Create Room
            </Button>
          </XStack>
        </YStack>
      </Card>
    </YStack>
  );
};
