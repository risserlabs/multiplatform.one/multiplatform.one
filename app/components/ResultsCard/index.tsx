/**
 * File: /components/ResultsCard/index.tsx
 * Project: app
 * File Created: 18-01-2025 10:08:11
 * Author: dharmendra
 * -----
 * BitSpur (c) Copyright 2021 - 2025
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { CheckCircle2, CircleX } from "@tamagui/lucide-icons";
import { Button, Card, H1, Separator, Text, XStack, YStack } from "ui";

export const ResultCard = () => {
  const correct = 7;
  const wrong = 3;
  return (
    <YStack
      ai="center"
      jc="center"
      paddingTop="$2"
      gap="$2"
      w="100%"
      maxWidth={500}
      minWidth={300}
    >
      <YStack gap="$4" ai="center" jc="center">
        <Text fontSize="$6" fontWeight="bold" $sm={{ fontSize: "$5" }}>
          Quiz Results
        </Text>

        <Text color="$gray10" fontSize={24}>
          John Doe
        </Text>
        <Text
          fontSize="$8"
          fontWeight="bold"
          $sm={{ fontSize: 30, letterSpacing: 0.5 }}
        >
          7/10
        </Text>
      </YStack>
      <YStack gap="$5" ai="center" jc="center" w="100%" $sm={{ gap: 4 }}>
        <XStack ai="center" jc="center" gap="$4">
          {/* Correct answers box */}
          <YStack
            ai="center"
            jc="center"
            width={150}
            padding="$2"
            borderRadius="$2"
            backgroundColor="$green2"
            gap="$2"
            borderColor="green"
            borderWidth={1}
            height={85}
            $sm={{ width: 100 }}
          >
            <XStack gap="$2">
              <CheckCircle2 color="green" />
              <Text color="green">Correct</Text>
            </XStack>
            <Text fontSize={24} fontWeight="bold" color="green">
              {correct}
            </Text>
          </YStack>

          <YStack
            ai="center"
            jc="center"
            width={150}
            padding="$2"
            height={85}
            borderRadius="$2"
            backgroundColor="$red4"
            borderColor="$red9"
            gap="$2"
            borderWidth={1}
            $sm={{ width: 100 }}
          >
            <XStack gap="$2">
              <CircleX color="red" />
              <Text color="red">Wrong</Text>
            </XStack>

            <Text color="red" fontWeight="bold" fontSize={24}>
              {wrong}
            </Text>
          </YStack>
        </XStack>
      </YStack>
      <Separator width="90%" marginVertical="$4" borderWidth={1} />
      <Text
        color="$color12"
        fontSize={18}
        $sm={{ fontSize: 14, letterSpacing: 0.2 }}
        textAlign="center"
        w="90%"
      >
        Great job! You've passed the quiz! 🎉
      </Text>
    </YStack>
  );
};
export default ResultCard;
