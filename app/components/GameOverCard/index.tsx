/**
 * File: /components/GameOverCard/index.tsx
 * Project: app
 * File Created: 18-01-2025 10:04:12
 *
 * BitSpur (c) Copyright 2021 - 2025
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { Home, Play } from "@tamagui/lucide-icons";
import { ResultCard } from "app/components/ResultsCard/index";
import { useRouter } from "one";
import { Button, Card, Text, XStack, YStack } from "ui";
export const GameOverCard = () => {
  const router = useRouter();
  return (
    <YStack
      ai="center"
      jc="center"
      paddingTop="$10"
      w="100%"
      maxWidth={500}
      minWidth={320}
    >
      <Card
        backgroundColor="$color1"
        borderRadius="$3"
        width="90%"
        maxWidth={500}
        minWidth={300}
        padding="$4"
        gap="$4"
        alignItems="center"
      >
        <Card.Header gap="$2">
          <Text
            fontSize="$10"
            fontWeight="bold"
            $sm={{ fontSize: "$5" }}
            textAlign="center"
          >
            Game Over
          </Text>
          <Text
            alignSelf="center"
            fontSize="$5"
            opacity={0.5}
            $sm={{ fontSize: "$4" }}
          >
            Thank you for playing!
          </Text>
        </Card.Header>
        <YStack width="100%" ai="center" jc="center">
          <ResultCard />
        </YStack>

        <XStack
          flex={1}
          gap="$4"
          $sm={{ gap: "$2" }}
          jc="space-between"
          w="90%"
        >
          <Button
            width="50%"
            fontWeight="bold"
            borderColor="$color12"
            borderRadius="$2"
            backgroundColor="$color1"
            hoverStyle={{
              backgroundColor: "$color10",
              scale: 1.05,
            }}
            padding="$4"
            $sm={{
              scale: 0.9,
              minWidth: "50%",
              w: "50%",
            }}
            onPress={() => router.push("/join-room")}
          >
            <Play size="$1" $sm={{ size: "$2" }} />
            <Text fontSize="$4" $sm={{ fontSize: "$3" }}>
              Replay quiz
            </Text>
          </Button>

          <Button
            fontWeight="bold"
            width="50%"
            borderRadius="$2"
            backgroundColor="$color12"
            color="$color1"
            borderColor="$color1"
            onPress={() => router.push("/")}
            hoverStyle={{
              backgroundColor: "$color10",
              scale: 1.05,
            }}
            padding="$4"
            $sm={{
              scale: 0.9,

              minWidth: "50%",
              w: "50%",
            }}
          >
            <Home color="$color1" size="$1" $sm={{ size: "$2" }} />
            <Text fontSize="$4" $sm={{ fontSize: "$3" }} color="$color1">
              Return Home
            </Text>
          </Button>
        </XStack>
      </Card>
    </YStack>
  );
};
