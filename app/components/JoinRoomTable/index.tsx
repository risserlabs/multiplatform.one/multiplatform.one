/**
 * File: /components/JoinRoomTable/index.tsx
 * Project: app
 * File Created: 18-01-2025 10:24:06
 *
 * -----
 * BitSpur (c) Copyright 2021 - 2025
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { useRouter } from "one";
import { Button, Dialog, Input, Label, Table, XStack, YStack } from "ui";

export const JoinRoomTable = () => {
  const router = useRouter();
  return (
    <YStack gap="$6">
      <XStack
        paddingTop="$4"
        justifyContent="flex-end"
        gap="$2"
        alignItems="center"
      >
        <Input size="$3" placeholder="Enter Room_ID" />
        <Button
          size="$2"
          color="$color1"
          backgroundColor="$color12"
          width="$6"
          hoverStyle={{
            backgroundColor: "$color11",
            scale: 1.05,
          }}
        >
          Search
        </Button>
      </XStack>
      <YStack>
        <Table heading="">
          <Table.Col>
            <Table.Cell fontSize="$3" backgroundColor="$color1">
              Room No
            </Table.Cell>
            <Table.Cell fontSize="$4">1</Table.Cell>
          </Table.Col>
          <Table.Col>
            <Table.Cell fontSize="$4" backgroundColor="$color1">
              Room Name
            </Table.Cell>
            <Table.Cell fontSize="$3">Game Room 1</Table.Cell>
          </Table.Col>
          <Table.Col>
            <Table.Cell fontSize="$4" backgroundColor="$color1">
              Room ID
            </Table.Cell>
            <Table.Cell fontSize="$3">ID_1</Table.Cell>
          </Table.Col>
          <Table.Col>
            <Table.Cell fontSize="$4" backgroundColor="$color1">
              Participants
            </Table.Cell>
            <Table.Cell fontSize="$3">2/4</Table.Cell>
          </Table.Col>
          <Table.Col>
            <Table.Cell fontSize="$4" backgroundColor="$color1">
              Action
            </Table.Cell>
            <Table.Cell>
              <Dialog>
                <Dialog.Trigger asChild>
                  <Button
                    size="$2"
                    width="70%"
                    color="$color1"
                    backgroundColor="$color12"
                    hoverStyle={{
                      backgroundColor: "$color11",
                      scale: 1.05,
                    }}
                  >
                    Join Room
                  </Button>
                </Dialog.Trigger>

                <Dialog.Portal>
                  <Dialog.Overlay
                    key="overlay"
                    animation="slow"
                    opacity={0.5}
                    enterStyle={{ opacity: 0 }}
                    exitStyle={{ opacity: 0 }}
                  />

                  <Dialog.Content
                    bordered
                    elevate
                    key="content"
                    width={450}
                    animateOnly={["transform", "opacity"]}
                    enterStyle={{ x: 0, y: -20, opacity: 0, scale: 0.9 }}
                    exitStyle={{ x: 0, y: 10, opacity: 0, scale: 0.95 }}
                    gap="$4"
                  >
                    <Dialog.Title>Join Room : Game Room 1</Dialog.Title>
                    <Dialog.Description>
                      Join a room and start playing
                    </Dialog.Description>
                    <YStack>
                      <YStack>
                        <Label>Nick Name</Label>
                        <Input placeholder="nick name" />
                      </YStack>
                    </YStack>
                    <XStack alignSelf="flex-end" gap="$4">
                      <Dialog.Close asChild>
                        <Button theme="active" aria-label="Close">
                          Cancel
                        </Button>
                      </Dialog.Close>
                      <Button
                        onPress={() => router.push("/join-room/1")}
                        theme="active"
                        aria-label="Close"
                        backgroundColor="$color12"
                        color="$color1"
                        hoverStyle={{
                          backgroundColor: "$color11",
                          scale: 1.05,
                        }}
                        fontWeight="bold"
                      >
                        Join
                      </Button>
                    </XStack>
                  </Dialog.Content>
                </Dialog.Portal>
              </Dialog>
            </Table.Cell>
          </Table.Col>
        </Table>
      </YStack>
    </YStack>
  );
};
