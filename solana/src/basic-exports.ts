/**
 * File: /anchor/src/basic-exports.ts
 * Project: solana
 * File Created: 13-01-2025 19:20:27
 * Author: Clay Risser
 * -----
 * BitSpur (c) Copyright 2021 - 2025
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { type AnchorProvider, Program } from "@coral-xyz/anchor";
import { PublicKey } from "@solana/web3.js";
import BasicIDL from "../target/idl/basic.json";
import type { Basic } from "../target/types/basic";

export const BASIC_PROGRAM_ID = new PublicKey(BasicIDL.address);

export function getBasicProgram(provider: AnchorProvider) {
  return new Program(BasicIDL as Basic, provider);
}

export { type Basic, BasicIDL };
