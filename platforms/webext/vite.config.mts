/*
 * File: /vite.config.mts
 * Project: React WebExt
 * File Created: 21-01-2025 14:49:02
 * Author: Clay Risser
 * -----
 * BitSpur (c) Copyright 2021 - 2025
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import fs from "node:fs";
import path from "node:path";
import {
  lookupTamaguiModules,
  resolveConfig,
} from "@multiplatform.one/utils/dev";
import react from "@vitejs/plugin-react";
import dotenv from "dotenv";
import type { ConfigEnv, UserConfig } from "vite";
import { defineConfig } from "vite";
import { public as publicConfigKeys } from "../../app/config.json";
import { isDev, port } from "./env";
import packageJson from "./package.json";

dotenv.config({ path: path.resolve(__dirname, "../../.env") });
process.env.VITE_MP_CONFIG = JSON.stringify(resolveConfig(publicConfigKeys));

const viewsDir = path.resolve(__dirname, "src/views");
const views = fs
  .readdirSync(viewsDir)
  .filter((file) => fs.statSync(path.join(viewsDir, file)).isDirectory());

export const sharedConfig: UserConfig = {
  root: path.resolve(__dirname),
  define: {
    __DEV__: isDev,
    __NAME__: JSON.stringify(packageJson.name),
    "process.env.NODE_ENV": JSON.stringify(
      isDev ? "development" : "production",
    ),
    global: "globalThis",
  },
  optimizeDeps: {
    include: [
      "react",
      "react-dom",
      "webextension-polyfill",
      "@tamagui/core",
      "@tamagui/web",
      "tamagui",
      "react-native-web",
      "@multiplatform.one/components",
    ],
    esbuildOptions: {
      resolveExtensions: [
        ".webext.ts",
        ".webext.tsx",
        ".webext.js",
        ".webext.jsx",
        ".web.ts",
        ".web.tsx",
        ".web.js",
        ".web.jsx",
        ".ts",
        ".tsx",
        ".js",
        ".jsx",
      ],
      mainFields: ["module", "main"],
    },
  },
  esbuild: {
    jsx: "automatic",
    target: "esnext",
  },
  resolve: {
    alias: {
      "react-native": "react-native-web",
      "@": path.resolve(__dirname),
      app: path.resolve(__dirname, "../../app"),
      ui: path.resolve(__dirname, "../../packages/ui"),
    },
    extensions: [
      ".webext.ts",
      ".webext.tsx",
      ".webext.js",
      ".webext.jsx",
      ".web.ts",
      ".web.tsx",
      ".web.js",
      ".web.jsx",
      ".ts",
      ".tsx",
      ".js",
      ".jsx",
    ],
    mainFields: ["browser", "module", "main"],
  },
};

export default defineConfig(
  async ({ command }: ConfigEnv): Promise<UserConfig> => {
    const [{ tamaguiPlugin }] = await Promise.all([
      import("@tamagui/vite-plugin"),
    ]);
    const isServe = command === "serve";
    return {
      ...sharedConfig,
      base: isServe ? `http://localhost:${port}/` : "/dist/",
      plugins: [
        react({
          jsxRuntime: "automatic",
        }),
        tamaguiPlugin({
          components: lookupTamaguiModules([__dirname]),
          config: "../../app/tamagui.config.ts",
          optimize: true,
          outputCSS: "./tamagui.css",
        }),
      ],
      server: {
        port,
        hmr: {
          host: "localhost",
          protocol: "ws",
          clientPort: port,
        },
        origin: `http://localhost:${port}`,
        cors: true,
        headers: {
          "Access-Control-Allow-Origin": "*",
        },
      },
      build: {
        watch: isDev ? {} : undefined,
        outDir: path.resolve(__dirname, "extension/dist"),
        emptyOutDir: false,
        sourcemap: isDev ? "inline" : false,
        terserOptions: {
          mangle: false,
        },
        rollupOptions: {
          input: Object.fromEntries(
            views.map((view) => [
              view,
              path.resolve(__dirname, "src/views", view, "index.html"),
            ]),
          ),
        },
      },
    };
  },
);
