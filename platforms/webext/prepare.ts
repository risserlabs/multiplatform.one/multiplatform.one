/**
 * File: /prepare.ts
 * Project: React WebExt
 * File Created: 10-01-2025 21:24:42
 * Author: Clay Risser
 * -----
 * BitSpur (c) Copyright 2021 - 2025
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import fs from "node:fs";
import path from "node:path";
import chokidar from "chokidar";
import { logger } from "multiplatform.one";
import { isDev, port } from "./env";
import { getManifest } from "./src/manifest";

function getViews() {
  const viewsDir = path.resolve(__dirname, "src/views");
  return fs
    .readdirSync(viewsDir)
    .filter((file) => fs.statSync(path.join(viewsDir, file)).isDirectory());
}

async function stubIndexHtml() {
  if (!isDev) return;
  const views = getViews();
  for (const view of views) {
    let data = await fs.promises.readFile(
      path.resolve(__dirname, "src/views", view, "index.html"),
      "utf-8",
    );
    data = data.replace(
      "</head>",
      `  <script type="module" src="http://localhost:${port}/@vite/client"></script>
  <script type="module" src="/dist/refresh.js"></script>
</head>`,
    );
    data = data
      .replace(
        '<script type="module" src="./main.tsx"></script>',
        `<script type="module" src="http://localhost:${port}/src/views/${view}/main.tsx"></script>`,
      )
      .replace(
        '<div id="app"></div>',
        '<div id="app">vite server did not start</div>',
      );
    await fs.promises.mkdir(
      path.dirname(
        path.resolve(__dirname, "extension/dist/src/views", view, "index.html"),
      ),
      { recursive: true },
    );
    await fs.promises.writeFile(
      path.resolve(__dirname, "extension/dist/src/views", view, "index.html"),
      data,
      "utf-8",
    );
  }
}

async function writeRefreshScript() {
  await fs.promises.mkdir(
    path.dirname(path.resolve(__dirname, "extension/dist/refresh.js")),
    { recursive: true },
  );
  await fs.promises.writeFile(
    path.resolve(__dirname, "extension/dist/refresh.js"),
    `window.__vite_plugin_react_preamble_installed__ = true;
window.$RefreshReg$ = () => {};
window.$RefreshSig$ = () => (type) => type;
window.addEventListener('load', () => {
  import("http://localhost:${port}/@react-refresh")
    .then((RefreshRuntime) => {
      if (RefreshRuntime && typeof RefreshRuntime.injectIntoGlobalHook === 'function') {
        RefreshRuntime.injectIntoGlobalHook(window);
      }
    })
    .catch(console.error);
});
`,
    "utf-8",
  );
}

export async function writeManifest() {
  await fs.promises.mkdir(
    path.dirname(path.resolve(__dirname, "extension/manifest.json")),
    { recursive: true },
  );
  await fs.promises.writeFile(
    path.resolve(__dirname, "extension/manifest.json"),
    JSON.stringify(await getManifest(), null, 2),
    "utf-8",
  );
}

(async () => {
  if (isDev) {
    logger.info("");
    await stubIndexHtml();
    await writeRefreshScript();
    await writeManifest();
    chokidar
      .watch(path.resolve(__dirname, "src/**/*.html"))
      .on("change", stubIndexHtml);
    chokidar
      .watch([
        path.resolve(__dirname, "src/manifest.ts"),
        path.resolve(__dirname, "package.json"),
      ])
      .on("change", writeManifest);
  } else {
    await writeManifest();
  }
})().catch(logger.error);
