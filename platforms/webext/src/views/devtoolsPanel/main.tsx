/**
 * File: /src/views/devtoolsPanel/main.tsx
 * Project: React WebExt
 * File Created: 15-01-2025 21:38:11
 * Author: Clay Risser
 * -----
 * BitSpur (c) Copyright 2021 - 2025
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import "../../../tamagui.css";
import "@tamagui/core/reset.css";
import { GlobalProvider } from "app/providers";
import tamaguiConfig from "app/tamagui.config";
import { StrictMode } from "react";
import { createRoot } from "react-dom/client";
import { H1, H2, Paragraph, YStack } from "ui";
import { ErrorBoundary } from "../../components/ErrorBoundary";

function DevtoolsPanel() {
  return (
    <YStack padding="$4">
      <H1 marginBottom="$4">Extension Debugger</H1>
      <YStack space="$4">
        <YStack>
          <H2 marginBottom="$2">Extension Info</H2>
          <Paragraph
            fontFamily="$mono"
            backgroundColor="$gray5"
            padding="$2"
            borderRadius="$2"
          >
            {JSON.stringify({ dev: __DEV__ }, null, 2)}
          </Paragraph>
        </YStack>
      </YStack>
    </YStack>
  );
}

const root = createRoot(document.getElementById("app")!);
root.render(
  <StrictMode>
    <ErrorBoundary>
      <GlobalProvider tamaguiConfig={tamaguiConfig}>
        <DevtoolsPanel />
      </GlobalProvider>
    </ErrorBoundary>
  </StrictMode>,
);
