/**
 * File: /src/manifest.ts
 * Project: React WebExt
 * File Created: 10-01-2025 21:24:42
 * Author: Clay Risser
 * -----
 * BitSpur (c) Copyright 2021 - 2025
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import fs from "node:fs";
import path from "node:path";
import { isFirefoxExtension } from "multiplatform.one";
import type { Manifest } from "webextension-polyfill";
import { isDev, port } from "../env";
import type PkgType from "../package.json";

export async function getManifest() {
  const pkg = JSON.parse(
    await fs.promises.readFile(
      path.resolve(__dirname, "../package.json"),
      "utf-8",
    ),
  ) as typeof PkgType;
  const manifest: Manifest.WebExtensionManifest = {
    manifest_version: 3,
    name: pkg.displayName || pkg.name,
    version: pkg.version,
    description: pkg.description,
    action: {
      default_icon: "./assets/icon-512.png",
      default_popup: "./dist/src/views/popup/index.html",
    },
    options_ui: {
      page: "./dist/src/views/options/index.html",
      open_in_tab: true,
    },
    background: isFirefoxExtension
      ? {
          scripts: ["./dist/background/index.mjs"],
          type: "module",
        }
      : {
          service_worker: "./dist/background/index.mjs",
        },
    icons: {
      16: "./assets/icon-512.png",
      48: "./assets/icon-512.png",
      128: "./assets/icon-512.png",
    },
    permissions: ["tabs", "storage", "activeTab", "sidePanel", "devtools"],
    host_permissions: ["*://*/*"],
    content_scripts: [
      {
        matches: ["<all_urls>"],
        js: ["./dist/contentScripts/index.global.js"],
      },
    ],
    web_accessible_resources: [
      {
        resources: [
          "./dist/contentScripts/style.css",
          ...(isDev ? ["http://localhost:*/", "./dist/refresh.js"] : []),
        ],
        matches: ["<all_urls>"],
      },
    ],
    content_security_policy: {
      extension_pages: isDev
        ? `script-src 'self' http://localhost:${port}; connect-src 'self' ws://localhost:${port} http://localhost:${port}; object-src 'self'`
        : "script-src 'self'; object-src 'self'",
    },
    devtools_page: "./dist/src/views/devtools/index.html",
  };
  if (isFirefoxExtension) {
    manifest.sidebar_action = {
      default_panel: "./dist/src/views/sidepanel/index.html",
      default_icon: "./assets/icon-512.png",
      default_title: "React WebExt Sidepanel",
      open_at_install: true,
    };
  } else {
    (manifest as any).side_panel = {
      default_path: "./dist/src/views/sidepanel/index.html",
      default_icon: "./assets/icon-512.png",
      default_title: "React WebExt Sidepanel",
    };
  }
  return manifest;
}
