/*
 * File: /vite.config.content.mts
 * Project: React WebExt
 * File Created: 21-01-2025 14:49:02
 * Author: Clay Risser
 * -----
 * BitSpur (c) Copyright 2021 - 2025
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import path from "node:path";
import { lookupTamaguiModules } from "@multiplatform.one/utils/dev";
import { defineConfig } from "vite";
import type { ConfigEnv, UserConfig } from "vite";
import { isDev } from "./env";
import packageJson from "./package.json";

export default defineConfig(async (_: ConfigEnv): Promise<UserConfig> => {
  const [{ tamaguiPlugin }] = await Promise.all([
    import("@tamagui/vite-plugin"),
  ]);
  return {
    define: {
      __DEV__: isDev,
      __NAME__: JSON.stringify(packageJson.name),
      "process.env.NODE_ENV": JSON.stringify(
        isDev ? "development" : "production",
      ),
      global: "globalThis",
      process: {
        env: {
          NODE_ENV: JSON.stringify(isDev ? "development" : "production"),
        },
      },
    },
    optimizeDeps: {
      include: [
        "react",
        "react-dom",
        "webextension-polyfill",
        "@tamagui/core",
        "@tamagui/web",
        "tamagui",
        "react-native-web",
        "@multiplatform.one/components",
      ],
      esbuildOptions: {
        resolveExtensions: [
          ".webext.ts",
          ".webext.tsx",
          ".webext.js",
          ".webext.jsx",
          ".web.ts",
          ".web.tsx",
          ".web.js",
          ".web.jsx",
          ".ts",
          ".tsx",
          ".js",
          ".jsx",
        ],
        mainFields: ["module", "main"],
      },
    },
    esbuild: {
      jsx: "automatic",
      target: "esnext",
    },
    plugins: [
      tamaguiPlugin({
        components: lookupTamaguiModules([__dirname]),
        config: "../../app/tamagui.config.ts",
        optimize: true,
        outputCSS: "./tamagui.content.css",
      }),
    ],
    resolve: {
      alias: {
        "react-native": "react-native-web",
        "@": path.resolve(__dirname),
        app: path.resolve(__dirname, "../../app"),
        ui: path.resolve(__dirname, "../../packages/ui"),
      },
      extensions: [
        ".webext.ts",
        ".webext.tsx",
        ".webext.js",
        ".webext.jsx",
        ".web.ts",
        ".web.tsx",
        ".web.js",
        ".web.jsx",
        ".ts",
        ".tsx",
        ".js",
        ".jsx",
      ],
      mainFields: ["browser", "module", "main"],
    },
    build: {
      watch: isDev ? {} : undefined,
      outDir: path.resolve(__dirname, "extension/dist/contentScripts"),
      cssCodeSplit: false,
      emptyOutDir: false,
      sourcemap: isDev ? "inline" : false,
      lib: {
        entry: path.resolve(__dirname, "src/contentScripts/index.tsx"),
        name: packageJson.name,
        formats: ["iife"],
      },
      rollupOptions: {
        output: {
          entryFileNames: "index.global.js",
          extend: true,
        },
      },
    },
  };
});
