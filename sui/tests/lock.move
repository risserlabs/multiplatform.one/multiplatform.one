#[test_only]
module sui::counter_tests {
    use sui::counter;

    #[test]
    fun test_counter() {
        let mut counter = counter::create();
        assert!(counter::value(&counter) == 0, 0);

        counter::increment(&mut counter);
        assert!(counter::value(&counter) == 1, 1);
    }
}
