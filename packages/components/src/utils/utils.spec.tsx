/**
 * File: /src/utils/utils.spec.tsx
 * Project: @multiplatform.one/components
 * File Created: 01-01-1970 00:00:00
 * Author: Tarun2811
 * -----
 * BitSpur (c) Copyright 2021 - 2025
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { config } from "@tamagui/config";
import { act, render } from "@testing-library/react";
import { TamaguiProvider, Text, createTamagui } from "tamagui";
import { beforeEach, describe, expect, it, vi } from "vitest";
import { isTinted, listeners, setTinted, toggleTinted } from "./setTinted";
import { unwrapText } from "./unwrapText";
const tamaguiConfig = createTamagui(config);

describe("setTinted", () => {
  const renderWithProviders = (ui: React.ReactElement) => {
    return render(
      <TamaguiProvider config={tamaguiConfig} defaultTheme="light">
        {ui}
      </TamaguiProvider>,
    );
  };
  beforeEach(() => {
    listeners.clear();
  });

  it("should set isTinted to the provided value", () => {
    setTinted(false);
    expect(isTinted).toBe(false);
    setTinted(true);
    expect(isTinted).toBe(true);
  });

  it("should call listeners when isTinted is set", () => {
    const listener = vi.fn();
    listeners.add(listener);
    setTinted(false);
    expect(listener).toHaveBeenCalledWith(false);
    setTinted(true);
    expect(listener).toHaveBeenCalledWith(true);
  });

  it("should toggle isTinted value", () => {
    expect(isTinted).toBe(true);
    toggleTinted();
    expect(isTinted).toBe(false);
    toggleTinted();
    expect(isTinted).toBe(true);
  });

  it("should notify listeners when toggled", () => {
    const listener = vi.fn();
    listeners.add(listener);
    toggleTinted();
    expect(listener).toHaveBeenCalledWith(false);
    toggleTinted();
    expect(listener).toHaveBeenCalledWith(true);
  });

  it("should handle invalid inputs gracefully", () => {
    const listener = vi.fn();
    listeners.add(listener);
  });
});

describe("unwrapText", () => {
  it("should return an array of text when passed a single Text component", () => {
    const result = unwrapText(<Text>Sample Text</Text>);
    expect(result).toEqual(["Sample Text"]);
  });

  it("should handle empty children", () => {
    const result = unwrapText(null);
    expect(result).toEqual([]);
  });

  it("should handle undefined children", () => {
    const result = unwrapText(undefined);
    expect(result).toEqual([]);
  });
});
