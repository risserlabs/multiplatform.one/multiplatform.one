/**
 * File: /src/panels/SimpleSheet/SimpleSheet.spec.tsx
 * Project: @multiplatform.one/components
 * File Created: 03-02-2025 15:58:08
 * Author: Bit Spur
 * -----
 * BitSpur (c) Copyright 2021 - 2025
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { config } from "@tamagui/config";
import { render } from "@testing-library/react";
import { TamaguiProvider, Text, YStack, createTamagui } from "tamagui";
import { SimpleSheet } from "./index";

const tamaguiConfig = createTamagui(config);

describe("SimpleSheet", () => {
  const renderWithProviders = (ui: React.ReactElement) => {
    return render(
      <TamaguiProvider config={tamaguiConfig} defaultTheme="light">
        {ui}
      </TamaguiProvider>,
    );
  };

  it("should render successfully with children", () => {
    const { getByText } = renderWithProviders(
      <SimpleSheet>
        <YStack>
          <Text>Test Child</Text>
        </YStack>
      </SimpleSheet>,
    );
    expect(getByText("Test Child")).toBeDefined();
  });

  it("should render correctly with custom props", () => {
    const { container } = renderWithProviders(
      <SimpleSheet zIndex={200}>
        <YStack>
          <Text>Test Child</Text>
        </YStack>
      </SimpleSheet>,
    );
    const sheet = container.querySelector("[data-frame]");
    expect(sheet).toHaveStyle("z-index: 200");
  });

  it("should apply frame styles", () => {
    const { container } = renderWithProviders(
      <SimpleSheet frameStyle={{ padding: "20px" }}>
        <YStack>
          <Text>Test Child</Text>
        </YStack>
      </SimpleSheet>,
    );
    const frame = container.querySelector("[data-frame]");
    expect(frame).toHaveStyle("padding: 20px 20px 20px 20px");
  });

  it("should render without crashing when no children are provided", () => {
    const { container } = renderWithProviders(<SimpleSheet />);
    expect(container).toBeDefined();
  });
});
