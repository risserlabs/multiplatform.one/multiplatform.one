/**
 * File: /src/forms/Switch/FieldSwitch.spec.tsx
 * Project: @multiplatform.one/components
 * File Created: 30-01-2024 10:03:48
 * Author: Clay Risser
 * -----
 * BitSpur (c) Copyright 2021 - 2024
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { config } from "@tamagui/config";
import { useForm } from "@tanstack/react-form";
import { act, fireEvent, render } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import { TamaguiProvider, createTamagui } from "tamagui";
import { describe, expect, it, vi } from "vitest";
import { Form } from "../Form";
import { FieldSwitch } from "./FieldSwitch";
import { Switch } from "./Switch";

const tamaguiConfig = createTamagui(config);

describe("FieldSwitch", () => {
  const renderWithProviders = (ui: React.ReactElement) => {
    return render(
      <TamaguiProvider config={tamaguiConfig} defaultTheme="light">
        {ui}
      </TamaguiProvider>,
    );
  };

  const defaultProps = {
    label: "Test Switch",
    name: "test",
  };
  const renderSwitch = (props = {}) => {
    return renderWithProviders(<FieldSwitch {...defaultProps} {...props} />);
  };
  const renderWithForm = (props = {}, formProps = {}) => {
    return renderWithProviders(
      <Form defaultValues={{ test: false }} {...formProps}>
        <FieldSwitch {...defaultProps} {...props} />
      </Form>,
    );
  };
  describe("FieldSwitch Rendering", () => {
    it("should render successfully", () => {
      const { container } = renderSwitch();
      expect(container).toBeDefined();
    });

    it("should render with label", () => {
      const { container } = renderSwitch({
        label: "Custom Label",
      });
      expect(container).toBeDefined();
    });

    it("should render with helper text", () => {
      const { container } = renderSwitch({
        helperText: "Helper text",
      });
      expect(container).toBeDefined();
    });
  });

  describe("Form Integration", () => {
    it("should handle form context", () => {
      const { container } = renderWithForm();
      expect(container).toBeDefined();
    });

    it("should handle default value", () => {
      const { container } = renderWithForm(
        { defaultValue: true },
        { defaultValues: { test: true } },
      );
      expect(container).toBeDefined();
    });

    it("should handle form submission", async () => {
      const onSubmit = vi.fn();
      const { container } = renderWithForm(
        {},
        {
          onSubmit: async (values) => {
            onSubmit(values);
            return undefined;
          },
        },
      );
      expect(container).toBeDefined();
      expect(onSubmit).toBeDefined();
    });
  });

  describe("Switch Event Handlers", () => {
    it("should handle blur events correctly", async () => {
      const customOnBlur = vi.fn();
      const { getByRole } = renderWithForm({
        onBlur: customOnBlur,
      });
      const switchElement = getByRole("switch");
      await act(async () => {
        await userEvent.tab();
        await userEvent.tab();
      });
      expect(customOnBlur).toHaveBeenCalled();
    });

    it("should handle checked changes correctly", async () => {
      const onCheckedChange = vi.fn();
      const { getByRole } = renderWithForm({
        onCheckedChange,
      });
      const switchElement = getByRole("switch");
      await act(async () => {
        await userEvent.click(switchElement);
      });
      expect(onCheckedChange).toHaveBeenCalledWith(true);
    });

    it("should update form value when switch is toggled", async () => {
      const onSubmit = vi.fn();
      const { getByRole } = renderWithForm(
        {},
        {
          onSubmit: async (values) => {
            onSubmit(values);
            return undefined;
          },
        },
      );
      const switchElement = getByRole("switch");
      await act(async () => {
        await userEvent.click(switchElement);
      });
      expect(switchElement).toHaveAttribute("aria-checked", "true");
    });

    it("should handle default values correctly", () => {
      const { getByRole } = renderWithForm(
        { defaultValue: true },
        { defaultValues: { test: true } },
      );
      const switchElement = getByRole("switch");
      expect(switchElement).toHaveAttribute("aria-checked", "true");
    });

    it("should handle both form and custom handlers", async () => {
      const customOnCheckedChange = vi.fn();
      const formOnChange = vi.fn();
      const { getByRole } = renderWithForm(
        {
          onCheckedChange: customOnCheckedChange,
        },
        {
          onChange: formOnChange,
        },
      );
      const switchElement = getByRole("switch");
      await act(async () => {
        await userEvent.click(switchElement);
      });
      expect(customOnCheckedChange).toHaveBeenCalledWith(true);
    });

    it("should maintain state after blur", async () => {
      const { getByRole } = renderWithForm();
      const switchElement = getByRole("switch");
      await act(async () => {
        await userEvent.click(switchElement);
      });
      await act(async () => {
        await userEvent.tab();
        await userEvent.tab();
      });
      expect(switchElement).toHaveAttribute("aria-checked", "true");
    });

    it("should handle multiple state changes", async () => {
      const onCheckedChange = vi.fn();
      const { getByRole } = renderWithForm({
        onCheckedChange,
      });
      const switchElement = getByRole("switch");
      await act(async () => {
        await userEvent.click(switchElement);
      });
      expect(onCheckedChange).toHaveBeenCalledWith(true);
      await act(async () => {
        await userEvent.click(switchElement);
      });
      expect(onCheckedChange).toHaveBeenCalledWith(false);
    });

    it("should handle disabled state", async () => {
      const onCheckedChange = vi.fn();
      const onBlur = vi.fn();
      const { getByRole } = renderWithForm({
        onCheckedChange,
        onBlur,
        disabled: true,
        switchProps: {
          disabled: true,
        },
      });
      const switchElement = getByRole("switch");
      expect(switchElement).toBeDisabled();
      await act(async () => {
        await userEvent.click(switchElement);
        await userEvent.tab();
        await userEvent.tab();
      });
      expect(onCheckedChange).not.toHaveBeenCalled();
      expect(onBlur).not.toHaveBeenCalled();
    });
  });

  describe("Switch without form or name", () => {
    it("should render switch without form context", () => {
      const { getByRole } = renderWithProviders(
        <FieldSwitch defaultValue={true} label="Test Switch" />,
      );
      const switchElement = getByRole("switch");
      expect(switchElement).toBeDefined();
      expect(switchElement).toHaveAttribute("aria-checked", "true");
    });

    it("should handle onBlur without form context", async () => {
      const onBlur = vi.fn();
      const { getByRole } = renderWithProviders(
        <FieldSwitch defaultValue={false} onBlur={onBlur} />,
      );
      const switchElement = getByRole("switch");
      await act(async () => {
        await userEvent.tab();
        await userEvent.tab();
      });
      expect(onBlur).toHaveBeenCalled();
    });

    it("should handle checked changes without form context", async () => {
      const onCheckedChange = vi.fn();
      const { getByRole } = renderWithProviders(
        <FieldSwitch defaultValue={false} onCheckedChange={onCheckedChange} />,
      );
      const switchElement = getByRole("switch");
      await act(async () => {
        await userEvent.click(switchElement);
      });
      expect(onCheckedChange).toHaveBeenCalledWith(true);
    });

    it("should handle initial checked state correctly", () => {
      const { getByRole } = renderWithProviders(
        <FieldSwitch checked={true} defaultValue={false} />,
      );
      const switchElement = getByRole("switch");
      expect(switchElement).toHaveAttribute("aria-checked", "true");
    });
  });
});
