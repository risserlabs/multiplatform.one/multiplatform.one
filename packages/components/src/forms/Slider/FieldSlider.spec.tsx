/**
 * File: /src/forms/Slider/FieldSlider.spec.tsx
 * Project: @multiplatform.one/components
 * File Created: 30-01-2025 11
 * -----
 * BitSpur (c) Copyright 2021 - 2025
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { config } from "@tamagui/config";
import { useForm } from "@tanstack/react-form";
import { act, fireEvent, render, waitFor } from "@testing-library/react";
import { TamaguiProvider, YStack, createTamagui } from "tamagui";
import { describe, expect, it, vi } from "vitest";
import { SubmitButton } from "../Button/SubmitButton";
import { Form } from "../Form";
import { FieldSlider } from "./FieldSlider";

const tamaguiConfig = createTamagui(config);

describe("FieldSlider", () => {
  const renderWithProviders = (ui: React.ReactElement) => {
    return render(
      <TamaguiProvider config={tamaguiConfig} defaultTheme="light">
        {ui}
      </TamaguiProvider>,
    );
  };

  describe("without form context", () => {
    it("should render with label and helper text correctly", () => {
      const { getByText, getByRole } = renderWithProviders(
        <FieldSlider
          label="Volume"
          helperText="Adjust the volume level"
          name="volume"
          defaultValue={[50]}
        />,
      );
      const slider = getByRole("slider");
      const label = getByText("Volume");
      const helperText = getByText("Adjust the volume level");
      expect(label).toBeDefined();
      expect(helperText).toBeDefined();
      expect(slider).toBeDefined();
      expect(slider.getAttribute("aria-valuenow")).toBe("50");
    });

    it("should handle slider value changes", () => {
      const onValueChange = vi.fn();
      const { getByRole } = renderWithProviders(
        <FieldSlider
          label="Volume"
          name="volume"
          onValueChange={onValueChange}
          defaultValue={[0]}
        />,
      );
      const slider = getByRole("slider");
      fireEvent.keyDown(slider, { key: "ArrowRight" });
      expect(onValueChange).toHaveBeenCalled();
    });

    it("should respect disabled state", () => {
      const onValueChange = vi.fn();
      const { getByRole } = renderWithProviders(
        <FieldSlider
          label="Volume"
          name="volume"
          disabled
          onValueChange={onValueChange}
          defaultValue={[50]}
        />,
      );
      const slider = getByRole("slider");
      expect(slider.getAttribute("aria-disabled")).toBe("true");
    });
  });

  describe("with form context", () => {
    const FormExample = ({
      defaultValue = [50],
      name = "volume",
      onSubmit = async (_value: any) => {},
    }) => {
      return (
        <Form
          defaultValues={{
            [name]: defaultValue,
          }}
          onSubmit={async ({ value }) => {
            await onSubmit(value);
          }}
        >
          <FieldSlider label="Volume" name={name} defaultValue={defaultValue} />
          <SubmitButton>Submit</SubmitButton>
        </Form>
      );
    };

    it("should render with form context and handle form submission", async () => {
      const onSubmit = vi.fn();
      const { getByRole, getByText } = renderWithProviders(
        <YStack>
          <FormExample onSubmit={onSubmit} />
        </YStack>,
      );
      const slider = getByRole("slider");
      const submitButton = getByText("Submit");
      expect(slider.getAttribute("aria-valuenow")).toBe("50");
      await act(async () => {
        fireEvent.keyDown(slider, { key: "ArrowRight" });
        fireEvent.click(submitButton);
      });
      expect(onSubmit).toHaveBeenCalled();
    });

    it("should handle disabled state in form context", () => {
      const { getByRole } = renderWithProviders(
        <Form defaultValues={{ volume: [50] }}>
          <FieldSlider label="Volume" name="volume" disabled />
        </Form>,
      );
      const slider = getByRole("slider");
      expect(slider.getAttribute("aria-disabled")).toBe("true");
    });

    it("should handle field errors from form validation", async () => {
      const TestComponent = () => {
        const form = useForm({
          defaultValues: {
            volume: [50],
          },
          onSubmit: async () => {},
        });
        return (
          <Form form={form}>
            <FieldSlider
              label="Volume"
              name="volume"
              validators={{
                onBlur: ({ value }: { value: number[] }) =>
                  value[0] > 80 ? "Volume cannot exceed 80" : undefined,
              }}
              defaultValue={[90]}
            />
          </Form>
        );
      };
      const { getByRole, findByText } = renderWithProviders(<TestComponent />);
      const slider = getByRole("slider");
      await act(async () => {
        fireEvent.blur(slider);
      });
      const errorMessage = await findByText("Volume cannot exceed 80");
      expect(errorMessage).toBeDefined();
    });

    it("should handle blur events correctly", async () => {
      const onBlur = vi.fn();
      const { getByRole } = renderWithProviders(
        <Form defaultValues={{ volume: [50] }}>
          <FieldSlider label="Volume" name="volume" onBlur={onBlur} />
        </Form>,
      );
      const slider = getByRole("slider");
      await act(async () => {
        fireEvent.blur(slider);
      });
      expect(onBlur).toHaveBeenCalled();
    });
  });

  describe("with useForm hook directly", () => {
    it("should handle form submission with useForm", async () => {
      const onSubmitMock = vi.fn();
      const TestComponent = () => {
        const form = useForm({
          defaultValues: {
            volume: [50],
          },
          onSubmit: async ({ value }) => {
            onSubmitMock(value);
          },
        });
        return (
          <Form form={form}>
            <FieldSlider label="Volume" name="volume" />
            <SubmitButton>Submit</SubmitButton>
          </Form>
        );
      };
      const { getByRole, getByText } = renderWithProviders(<TestComponent />);
      const slider = getByRole("slider");
      const submitButton = getByText("Submit");
      await act(async () => {
        fireEvent.keyDown(slider, { key: "ArrowRight" });
        fireEvent.click(submitButton);
      });
      expect(onSubmitMock).toHaveBeenCalled();
    });
  });

  it("should display error message when provided", () => {
    const { getByText } = renderWithProviders(
      <FieldSlider
        label="Volume"
        name="volume"
        defaultValue={[50]}
        error="Invalid volume level"
      />,
    );
    expect(getByText("Invalid volume level")).toBeDefined();
  });

  it("should handle min and max values", () => {
    const { getByRole } = renderWithProviders(
      <FieldSlider
        label="Volume"
        name="volume"
        defaultValue={[50]}
        sliderProps={{
          min: 0,
          max: 100,
        }}
      />,
    );
    const slider = getByRole("slider");
    expect(slider.getAttribute("aria-valuemin")).toBe("0");
    expect(slider.getAttribute("aria-valuemax")).toBe("100");
  });

  describe("Slider without form or name", () => {
    it("should render without form context", () => {
      const { getByRole } = renderWithProviders(
        <FieldSlider defaultValue={[50]} />,
      );
      const slider = getByRole("slider");
      expect(slider).toBeDefined();
      expect(slider.getAttribute("aria-valuenow")).toBe("50");
    });

    it("should handle onBlur without form context", async () => {
      const onBlur = vi.fn();
      const { getByRole } = renderWithProviders(
        <FieldSlider defaultValue={[50]} onBlur={onBlur} />,
      );
      const slider = getByRole("slider");
      await act(async () => {
        fireEvent.blur(slider);
      });
      expect(onBlur).toHaveBeenCalled();
    });

    it("should pass through fieldProps without form context", () => {
      const { getByText } = renderWithProviders(
        <FieldSlider
          defaultValue={[50]}
          label="Test Label"
          helperText="Helper Text"
        />,
      );
      expect(getByText("Test Label")).toBeDefined();
      expect(getByText("Helper Text")).toBeDefined();
    });

    it("should handle slider props without form context", () => {
      const { getByRole } = renderWithProviders(
        <FieldSlider
          defaultValue={[50]}
          sliderProps={{
            min: 0,
            max: 200,
            step: 10,
          }}
        />,
      );
      const slider = getByRole("slider");
      expect(slider.getAttribute("aria-valuemin")).toBe("0");
      expect(slider.getAttribute("aria-valuemax")).toBe("200");
    });

    it("should handle thumb props without form context", () => {
      const { getByRole } = renderWithProviders(
        <FieldSlider
          defaultValue={[50]}
          thumbProps={{
            index: 0,
            size: "$4",
            "aria-label": "Custom thumb",
          }}
        />,
      );
      const thumb = getByRole("slider");
      expect(thumb).toHaveAttribute("aria-label", "Custom thumb");
    });

    it("should handle value changes without form context", async () => {
      const onValueChange = vi.fn();
      const { getByRole } = renderWithProviders(
        <FieldSlider defaultValue={[30]} onValueChange={onValueChange} />,
      );
      const slider = getByRole("slider");
      await act(async () => {
        fireEvent.keyDown(slider, { key: "ArrowRight" });
      });
      expect(onValueChange).toHaveBeenCalled();
    });

    it("should handle disabled state without form context", () => {
      const { getByRole } = renderWithProviders(
        <FieldSlider defaultValue={[50]} disabled />,
      );
      const slider = getByRole("slider");
      expect(slider).toHaveAttribute("aria-disabled", "true");
    });
  });
});
