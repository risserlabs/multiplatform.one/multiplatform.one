/**
 * File: /src/forms/Select/FieldSelectSimple.spec.tsx
 * Project: @multiplatform.one/components
 * File Created: 31-01-2025 05:05:03
 * Author: sachinsuralkar
 * -----
 * BitSpur (c) Copyright 2021 - 2025
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { act, render } from "@testing-library/react";
import { describe, expect, it, vi } from "vitest";
import { Form } from "../Form";
import { FieldSelectSimple } from "./FieldSelectSimple";
import { Select } from "./Select";

describe("FieldSelectSimple", () => {
  it("should render successfully", () => {
    const { container } = render(
      <FieldSelectSimple label="Test Select" name="test">
        <Select.Item index={0} value="1">
          Option 1
        </Select.Item>
        <Select.Item index={1} value="2">
          Option 2
        </Select.Item>
      </FieldSelectSimple>,
    );
    expect(container).toBeDefined();
  });

  it("should render within form context", () => {
    const { container } = render(
      <Form defaultValues={{ test: "" }}>
        <FieldSelectSimple name="test" label="Test Select">
          <Select.Item index={0} value="1">
            Option 1
          </Select.Item>
          <Select.Item index={1} value="2">
            Option 2
          </Select.Item>
        </FieldSelectSimple>
      </Form>,
    );
    expect(container).toBeDefined();
  });

  it("should render with placeholder", () => {
    const { container } = render(
      <FieldSelectSimple
        label="Test Select"
        name="test"
        placeholder="Select an option"
      >
        <Select.Item index={0} value="1">
          Option 1
        </Select.Item>
        <Select.Item index={1} value="2">
          Option 2
        </Select.Item>
      </FieldSelectSimple>,
    );
    expect(container).toBeDefined();
  });

  it("should render with default value", () => {
    const { container } = render(
      <Form defaultValues={{ test: "1" }}>
        <FieldSelectSimple name="test" label="Test Select" defaultValue="1">
          <Select.Item index={0} value="1">
            Option 1
          </Select.Item>
          <Select.Item index={1} value="2">
            Option 2
          </Select.Item>
        </FieldSelectSimple>
      </Form>,
    );
    expect(container).toBeDefined();
  });

  it("should render with error state", () => {
    const { container } = render(
      <FieldSelectSimple
        label="Test Select"
        name="test"
        error="This is an error"
      >
        <Select.Item index={0} value="1">
          Option 1
        </Select.Item>
        <Select.Item index={1} value="2">
          Option 2
        </Select.Item>
      </FieldSelectSimple>,
    );
    expect(container).toBeDefined();
  });

  it("should render with custom select props", () => {
    const { container } = render(
      <FieldSelectSimple
        label="Test Select"
        name="test"
        selectProps={{ disabled: true }}
      >
        <Select.Item index={0} value="1">
          Option 1
        </Select.Item>
        <Select.Item index={1} value="2">
          Option 2
        </Select.Item>
      </FieldSelectSimple>,
    );
    expect(container).toBeDefined();
  });

  it("should handle value changes", () => {
    const onValueChange = vi.fn();
    const { container } = render(
      <Form defaultValues={{ test: "" }}>
        <FieldSelectSimple
          name="test"
          label="Test Select"
          onValueChange={onValueChange}
        >
          <Select.Item index={0} value="1">
            Option 1
          </Select.Item>
          <Select.Item index={1} value="2">
            Option 2
          </Select.Item>
        </FieldSelectSimple>
      </Form>,
    );
    expect(container).toBeDefined();
  });

  it("should handle form submission", async () => {
    const onSubmit = vi.fn();
    const { container } = render(
      <Form
        defaultValues={{ test: "" }}
        onSubmit={async (values) => {
          onSubmit(values);
          return undefined;
        }}
      >
        <FieldSelectSimple name="test" label="Test Select">
          <Select.Item index={0} value="1">
            Option 1
          </Select.Item>
          <Select.Item index={1} value="2">
            Option 2
          </Select.Item>
        </FieldSelectSimple>
        <button type="submit">Submit</button>
      </Form>,
    );
    expect(container).toBeDefined();
  });

  it("should handle onOpenChange callback", () => {
    const onOpenChange = vi.fn();
    const { container } = render(
      <FieldSelectSimple
        name="test"
        label="Test Select"
        onOpenChange={onOpenChange}
      >
        <Select.Item index={0} value="1">
          Option 1
        </Select.Item>
        <Select.Item index={1} value="2">
          Option 2
        </Select.Item>
      </FieldSelectSimple>,
    );
    expect(container).toBeDefined();
  });
});
