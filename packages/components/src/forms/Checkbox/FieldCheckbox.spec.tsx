/**
 * File: /src/forms/Checkbox/FieldCheckbox.spec.tsx
 * Project: @multiplatform.one/components
 * File Created: 30-12-2024 18:20:00
 * Author: Clay Risser
 * -----
 * BitSpur (c) Copyright 2021 - 2024
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { config } from "@tamagui/config";
import { useForm } from "@tanstack/react-form";
import { act, fireEvent, render, screen } from "@testing-library/react";
import { TamaguiProvider, YStack, createTamagui } from "tamagui";
import { describe, expect, it, vi } from "vitest";
import { SubmitButton } from "../Button/SubmitButton";
import { Form } from "../Form";
import { FieldCheckbox } from "./FieldCheckbox";

const tamaguiConfig = createTamagui(config);

describe("FieldCheckbox", () => {
  const renderWithProviders = (ui: React.ReactElement) => {
    return render(
      <TamaguiProvider config={tamaguiConfig} defaultTheme="light">
        {ui}
      </TamaguiProvider>,
    );
  };

  describe("without form context", () => {
    it("should render with label and helper text correctly", () => {
      const { getByText, getByRole } = renderWithProviders(
        <FieldCheckbox
          label="Hello"
          helperText="please check this box"
          name="test"
          defaultValue={false}
        />,
      );
      const checkbox = getByRole("checkbox");
      const label = getByText("Hello");
      const helperText = getByText("please check this box");
      expect(label).toBeDefined();
      expect(helperText).toBeDefined();
      expect(checkbox).not.toBeChecked();
      expect(checkbox).toBeEnabled();
    });

    it("should handle checkbox state changes", () => {
      const onCheckedChange = vi.fn();
      const { getByRole } = renderWithProviders(
        <FieldCheckbox
          label="Hello"
          name="test"
          onCheckedChange={onCheckedChange}
          defaultValue={false}
        />,
      );
      const checkbox = getByRole("checkbox");
      expect(checkbox).not.toBeChecked();
      fireEvent.click(checkbox);
      expect(onCheckedChange).toHaveBeenCalledWith(true);
    });

    it("should handle disabled state", () => {
      const onCheckedChange = vi.fn();
      const { getByRole } = renderWithProviders(
        <FieldCheckbox
          label="Hello"
          name="test"
          onCheckedChange={onCheckedChange}
          defaultValue={false}
          disabled
        />,
      );
      const checkbox = getByRole("checkbox");
      fireEvent.click(checkbox);
      expect(onCheckedChange).not.toHaveBeenCalled();
      expect(checkbox).toBeDisabled();
    });

    it("should display error message when error prop is passed", () => {
      const { getByText } = renderWithProviders(
        <FieldCheckbox
          label="Hello"
          name="test"
          error="This field is required"
          defaultValue={false}
        />,
      );
      const errorMessage = getByText("This field is required");
      expect(errorMessage).toBeDefined();
    });

    it("should handle indeterminate state", () => {
      const { getByRole, container } = renderWithProviders(
        <FieldCheckbox name="test" defaultValue="indeterminate" />,
      );
      const checkbox = getByRole("checkbox");
      expect(checkbox).toHaveAttribute("data-state", "indeterminate");
      expect(container.querySelector("svg")).toBeDefined();
    });

    it("should render required label with asterisk", () => {
      const { getByText } = renderWithProviders(
        <FieldCheckbox label="Required Field" name="test" required />,
      );
      const labelWithAsterisk = getByText("Required Field *");
      expect(labelWithAsterisk).toBeDefined();
    });

    it("should handle custom size prop", () => {
      const { getByText } = renderWithProviders(
        <FieldCheckbox
          label="Custom Size"
          name="test"
          size="$4"
          helperText="Helper text with custom size"
        />,
      );
      const label = getByText("Custom Size");
      const helperText = getByText("Helper text with custom size");
      expect(label).toHaveStyle({ fontSize: expect.any(String) });
      expect(helperText).toHaveStyle({ fontSize: expect.any(String) });
    });

    it("should handle custom children prop", () => {
      const CustomIndicator = () => (
        <div data-testid="custom-indicator">Custom</div>
      );
      const { getByTestId } = renderWithProviders(
        <FieldCheckbox name="test">
          <CustomIndicator />
        </FieldCheckbox>,
      );
      expect(getByTestId("custom-indicator")).toBeDefined();
    });

    it("should handle onBlur event", () => {
      const onBlurMock = vi.fn();
      const { getByRole } = renderWithProviders(
        <FieldCheckbox name="test" onBlur={onBlurMock} />,
      );
      const checkbox = getByRole("checkbox");
      fireEvent.blur(checkbox);
      expect(onBlurMock).toHaveBeenCalled();
    });

    it("should handle custom label props", () => {
      const customLabelProps = {
        style: { fontWeight: "bold" },
        "data-testid": "custom-label",
      };
      const { getByTestId } = renderWithProviders(
        <FieldCheckbox
          label="Custom Label"
          name="test"
          labelProps={customLabelProps}
        />,
      );
      const label = getByTestId("custom-label");
      expect(label).toBeDefined();
      expect(label).toHaveStyle({ fontWeight: "bold" });
    });

    it("should handle checkbox props", () => {
      const checkboxProps = {
        "data-testid": "custom-checkbox",
        style: { margin: "10px" },
      };
      const { getByTestId } = renderWithProviders(
        <FieldCheckbox name="test" checkboxProps={checkboxProps} />,
      );
      const checkbox = getByTestId("custom-checkbox");
      expect(checkbox).toBeDefined();
      expect(checkbox).toHaveStyle({ margin: "10px" });
    });

    it("should handle state transitions", () => {
      const onCheckedChange = vi.fn();
      const { getByRole } = renderWithProviders(
        <FieldCheckbox
          name="test"
          defaultValue={false}
          onCheckedChange={onCheckedChange}
        />,
      );
      const checkbox = getByRole("checkbox");
      fireEvent.click(checkbox);
      expect(onCheckedChange).toHaveBeenCalledWith(true);
      fireEvent.click(checkbox);
      expect(onCheckedChange).toHaveBeenCalledWith(false);
    });

    it("should handle error state styling", () => {
      const { getByText, getByRole } = renderWithProviders(
        <FieldCheckbox
          label="Error Field"
          name="test"
          error="Error message"
          helperText="This will be replaced by error"
        />,
      );
      const errorMessage = getByText("Error message");
      const checkbox = getByRole("checkbox");
      const helperText = screen.queryByText("This will be replaced by error");
      expect(errorMessage).toBeInTheDocument();
      expect(helperText).not.toBeInTheDocument();
      expect(checkbox).not.toBeChecked();
      fireEvent.click(checkbox);
      expect(checkbox).toBeChecked();
    });

    it("should render correctly without form and name props", () => {
      const onCheckedChange = vi.fn();
      const { getByRole } = renderWithProviders(
        <FieldCheckbox
          label="Test"
          defaultValue={false}
          onCheckedChange={onCheckedChange}
        />,
      );
      const checkbox = getByRole("checkbox");
      fireEvent.click(checkbox);
      expect(onCheckedChange).toHaveBeenCalledWith(true);
    });

    it("should handle all visual states without form context", () => {
      const { getByRole, getByText } = renderWithProviders(
        <FieldCheckbox
          label="Test Label"
          defaultValue={false}
          helperText="Helper message"
          size="$4"
          required
        />,
      );
      const checkbox = getByRole("checkbox");
      const label = getByText("Test Label *");
      const helperText = getByText("Helper message");
      expect(checkbox).toBeDefined();
      expect(label).toBeDefined();
      expect(helperText).toBeDefined();
    });

    it("should handle error state and hide helper text", () => {
      const { getByRole, getByText, queryByText } = renderWithProviders(
        <FieldCheckbox
          label="Test Label"
          defaultValue={false}
          helperText="Helper message"
          error="Error message"
          size="$4"
          required
        />,
      );
      const checkbox = getByRole("checkbox");
      const label = getByText("Test Label *");
      const errorText = getByText("Error message");
      const helperText = queryByText("Helper message");
      expect(checkbox).toBeDefined();
      expect(label).toBeDefined();
      expect(errorText).toBeDefined();
      expect(helperText).toBeNull();
    });

    it("should handle custom id prop", () => {
      const customId = "custom-checkbox-id";
      const { getByRole } = renderWithProviders(
        <FieldCheckbox id={customId} label="Test" defaultValue={false} />,
      );
      const checkbox = getByRole("checkbox");
      expect(checkbox.id).toBe(customId);
    });

    it("should handle different checkbox states without form context", () => {
      const { getByRole, rerender } = renderWithProviders(
        <FieldCheckbox defaultValue={false} />,
      );
      let checkbox = getByRole("checkbox");
      expect(checkbox).not.toBeChecked();
      rerender(<FieldCheckbox defaultValue={true} />);
      checkbox = getByRole("checkbox");
      expect(checkbox).toBeChecked();
      rerender(<FieldCheckbox defaultValue="indeterminate" />);
      checkbox = getByRole("checkbox");
      expect(checkbox).toHaveAttribute("data-state", "indeterminate");
    });

    it("should handle disabled state with checked value", () => {
      const onCheckedChange = vi.fn();
      const { getByRole } = renderWithProviders(
        <FieldCheckbox
          defaultValue={true}
          disabled
          onCheckedChange={onCheckedChange}
        />,
      );
      const checkbox = getByRole("checkbox");
      expect(checkbox).toBeChecked();
      expect(checkbox).toBeDisabled();
      fireEvent.click(checkbox);
      expect(onCheckedChange).not.toHaveBeenCalled();
    });
  });

  describe("with form context", () => {
    const FormExample = ({
      defaultValue = false,
      name = "foo",
      onSubmit = async (_value: any) => {},
    }) => {
      return (
        <Form
          defaultValues={{
            [name]: defaultValue,
          }}
          onSubmit={async ({ value }) => {
            await onSubmit(value);
          }}
        >
          <FieldCheckbox label="Accept" name={name} />
          <SubmitButton>Submit</SubmitButton>
        </Form>
      );
    };

    it("should render with form context and handle form submission", async () => {
      const onSubmit = vi.fn();
      const { getByText, getByRole } = renderWithProviders(
        <YStack>
          <FormExample onSubmit={onSubmit} />
        </YStack>,
      );
      const checkbox = getByRole("checkbox");
      const submitButton = getByText("Submit");
      expect(checkbox).not.toBeChecked();
      await act(async () => {
        fireEvent.click(submitButton);
      });
      expect(onSubmit).toHaveBeenCalledWith({ foo: false });
      await act(async () => {
        fireEvent.click(checkbox);
        fireEvent.click(submitButton);
      });
      expect(onSubmit).toHaveBeenCalledWith({ foo: true });
    });

    it("should handle disabled state in form context", () => {
      const onCheckedChange = vi.fn();
      const { getByRole } = renderWithProviders(
        <Form defaultValues={{ terms: false }}>
          <FieldCheckbox
            label="Accept"
            name="terms"
            disabled
            onCheckedChange={onCheckedChange}
          />
        </Form>,
      );
      const checkbox = getByRole("checkbox");
      fireEvent.click(checkbox);
      expect(onCheckedChange).not.toHaveBeenCalled();
      expect(checkbox).not.toBeChecked();
      expect(checkbox).toBeDisabled();
    });

    it("should display error message when error prop is passed in form context", async () => {
      const onSubmit = vi.fn();
      const { getByText, getByRole } = renderWithProviders(
        <YStack>
          <FormExample onSubmit={onSubmit} />
          <FieldCheckbox
            label="Accept"
            name="foo"
            error="This field is required"
          />
        </YStack>,
      );
      const errorMessage = getByText("This field is required");
      expect(errorMessage).toBeDefined();
    });
  });

  describe("with useForm hook directly", () => {
    it("should handle form submission with useForm", async () => {
      const onSubmitMock = vi.fn();
      const TestComponent = () => {
        const form = useForm({
          defaultValues: {
            terms: false,
          },
          onSubmit: async ({ value }) => {
            onSubmitMock(value);
          },
        });
        return (
          <Form form={form}>
            <FieldCheckbox label="Accept Terms" name="terms" />
            <SubmitButton>Submit</SubmitButton>
          </Form>
        );
      };
      const { getByRole, getByText } = renderWithProviders(<TestComponent />);
      const checkbox = getByRole("checkbox");
      const submitButton = getByText("Submit");
      await act(async () => {
        fireEvent.click(submitButton);
      });
      expect(onSubmitMock).toHaveBeenCalledWith({ terms: false });
      await act(async () => {
        fireEvent.click(checkbox);
        fireEvent.click(submitButton);
      });
      expect(onSubmitMock).toHaveBeenCalledWith({ terms: true });
    });
  });
});
