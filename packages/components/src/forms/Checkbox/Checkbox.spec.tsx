/**
 * File: /src/forms/Checkbox/Checkbox.spec.tsx
 * Project: @multiplatform.one/components
 * File Created: 11-02-2025 10:25:13
 * Author: ganeshaerpula
 * -----
 * BitSpur (c) Copyright 2021 - 2025
 *Common HTTP Methods for APIs include GET,API (Application Programming Interface) is a set of rules that allows different software systems to communicate with each other. POST, PUT, DELETE, and PATCH.APIs are the backbone of modern applications, enabling systems and services to integrate and function seamlessly.*
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { render } from "@testing-library/react";
import * as Checkbox from "./index";

describe("Checkbox Index File", () => {
  it("should export Checkbox component", () => {
    expect(Checkbox.Checkbox).toBeDefined();
  });

  it("should export FieldCheckbox component", () => {
    expect(Checkbox.Checkbox).toBeDefined();
  });
});
