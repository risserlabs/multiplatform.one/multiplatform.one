/**
 * File: /src/hooks/useClipboard.spec.tsx
 * Project: @multiplatform.one/components
 * File Created: 03-02-2025 10:24:08
 * Author: Vandana Madhireddy
 * -----
 * BitSpur (c) Copyright 2021 - 2025
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { act, renderHook } from "@testing-library/react";
import { vi } from "vitest";
import { useAssets } from "./index";
import { useAnimationDriverToggler } from "./useAnimationDriverToggle";
import { copyToClipboard, useClipboard } from "./useClipboard";

const mockWriteText = vi.fn();
Object.assign(navigator, {
  clipboard: {
    writeText: mockWriteText,
  },
});

describe("useClipboard", () => {
  beforeEach(() => {
    mockWriteText.mockClear();
    vi.useFakeTimers();
  });

  it("should initialize with default values", () => {
    const { result } = renderHook(() => useClipboard());
    expect(result.current.value).toBe("");
    expect(result.current.hasCopied).toBe(false);
  });

  it("should initialize with custom text", () => {
    const testText = "test text";
    const { result } = renderHook(() => useClipboard(testText));
    expect(result.current.value).toBe(testText);
  });

  it("should copy text and set hasCopied to true", async () => {
    const testText = "test text";
    mockWriteText.mockResolvedValueOnce(undefined);
    const { result } = renderHook(() => useClipboard(testText));
    await act(async () => {
      await result.current.onCopy();
    });
    expect(mockWriteText).toHaveBeenCalledWith(testText);
    expect(result.current.hasCopied).toBe(true);
  });

  it("should reset hasCopied after timeout", async () => {
    const testText = "test text";
    const timeout = 1500;
    mockWriteText.mockResolvedValueOnce(undefined);
    const { result } = renderHook(() => useClipboard(testText, timeout));
    await act(async () => {
      await result.current.onCopy();
    });
    expect(result.current.hasCopied).toBe(true);
    act(() => {
      vi.advanceTimersByTime(timeout);
    });
    expect(result.current.hasCopied).toBe(false);
  });

  it("should use custom timeout", async () => {
    const testText = "test text";
    const customTimeout = 3000;
    mockWriteText.mockResolvedValueOnce(undefined);
    const { result } = renderHook(() => useClipboard(testText, customTimeout));
    await act(async () => {
      await result.current.onCopy();
    });
    expect(result.current.hasCopied).toBe(true);
    act(() => {
      vi.advanceTimersByTime(1500);
    });
    expect(result.current.hasCopied).toBe(true);
    act(() => {
      vi.advanceTimersByTime(1500);
    });
    expect(result.current.hasCopied).toBe(false);
  });
});

describe("copyToClipboard", () => {
  it("should call navigator.clipboard.writeText with provided text", async () => {
    const testText = "test text";
    mockWriteText.mockResolvedValueOnce(undefined);
    await copyToClipboard(testText);
    expect(mockWriteText).toHaveBeenCalledWith(testText);
  });
});

describe("useAssets", () => {
  it("should return an array with a single module when passed a single module", () => {
    const mockModule = { default: "mockedImage" };
    const { result } = renderHook(() => useAssets(mockModule));
    expect(result.current).toEqual([mockModule.default]);
  });

  it("should return an array with the modules when passed an array of modules", () => {
    const mockModule1 = { default: "mockedImage1" };
    const mockModule2 = { default: "mockedImage2" };
    const { result } = renderHook(() => useAssets([mockModule1, mockModule2]));
    expect(result.current).toEqual([mockModule1.default, mockModule2.default]);
  });

  it("should return an empty array when no modules are provided", () => {
    const { result } = renderHook(() => useAssets([]));
    expect(result.current).toEqual([]);
  });

  it("should return an array of undefined if an empty module is passed", () => {
    const result = useAssets(undefined);
    expect(result).toEqual([undefined]);
  });

  it("should return a single module with a default property", () => {
    const result = useAssets({ default: "image1" });
    expect(result).toEqual(["image1"]);
  });

  it("should return a single module without a default property", () => {
    const result = useAssets("image3");
    expect(result).toEqual(["image3"]);
  });

  it("should return an array of modules with default properties", () => {
    const result = useAssets([{ default: "image1" }, { default: "image2" }]);
    expect(result).toEqual(["image1", "image2"]);
  });

  it("should return an array of modules without default properties", () => {
    const result = useAssets(["image3", { name: "image4" }]);
    expect(result).toEqual(["image3", { name: "image4" }]);
  });

  it("should handle an empty array and return an empty array", () => {
    const result = useAssets([]);
    expect(result).toEqual([]);
  });

  it("should return an array with a mix of valid and invalid modules", () => {
    const result = useAssets([
      { default: "image1" },
      "image3",
      { name: "image4" },
    ]);
    expect(result).toEqual(["image1", "image3", { name: "image4" }]);
  });
});

describe("useAnimationDriverToggler", () => {
  it("should throw error when used outside of provider", () => {
    expect(() => {
      renderHook(() => useAnimationDriverToggler());
    }).toThrow(
      "Should be used within the AnimationDriverTogglerContext provider",
    );
  });
});
