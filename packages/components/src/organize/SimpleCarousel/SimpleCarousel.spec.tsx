/**
 * File: /src/organize/SimpleCarousel/SimpleCarousel.spec.tsx
 * Project: @multiplatform.one/components
 * File Created: 01-01-1970 00:00:00
 * Author: abhiramkaleru
 * -----
 * BitSpur (c) Copyright 2021 - 2025
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { act, fireEvent, render, screen } from "@testing-library/react";
import { Text, YStack } from "tamagui";
import { afterEach, beforeEach, describe, expect, it, vi } from "vitest";
import { SimpleCarousel } from "./index";

describe("SimpleCarousel Component", () => {
  beforeEach(() => {
    vi.useFakeTimers();
  });
  afterEach(() => {
    vi.runOnlyPendingTimers();
    vi.useRealTimers();
  });

  it("should render successfully with children", () => {
    render(
      <SimpleCarousel>
        <YStack>
          <Text>Slide 1</Text>
        </YStack>
        <YStack>
          <Text>Slide 2</Text>
        </YStack>
        <YStack>
          <Text>Slide 3</Text>
        </YStack>
      </SimpleCarousel>,
    );
    expect(screen.getByText("Slide 1")).toBeInTheDocument();
  });

  it("should show the correct initial slide", () => {
    render(
      <SimpleCarousel>
        <YStack>
          <Text>Slide 1</Text>
        </YStack>
        <YStack>
          <Text>Slide 2</Text>
        </YStack>
        <YStack>
          <Text>Slide 3</Text>
        </YStack>
      </SimpleCarousel>,
    );
    expect(screen.getByText("Slide 1")).toBeInTheDocument();
  });

  it("should automatically change slides", () => {
    render(
      <SimpleCarousel speed={1000}>
        <YStack>
          <Text>Slide 1</Text>
        </YStack>
        <YStack>
          <Text>Slide 2</Text>
        </YStack>
        <YStack>
          <Text>Slide 3</Text>
        </YStack>
      </SimpleCarousel>,
    );
    act(() => {
      vi.advanceTimersByTime(1000);
    });
    expect(screen.getByText("Slide 2")).toBeInTheDocument();

    act(() => {
      vi.advanceTimersByTime(1000);
    });
    expect(screen.getByText("Slide 3")).toBeInTheDocument();

    act(() => {
      vi.advanceTimersByTime(1000);
    });
    expect(screen.getByText("Slide 1")).toBeInTheDocument();
  });

  it("should not show center indicators when disabled", () => {
    render(
      <SimpleCarousel showCenterIndicator={false}>
        <YStack>
          <Text>Slide 1</Text>
        </YStack>
        <YStack>
          <Text>Slide 2</Text>
        </YStack>
        <YStack>
          <Text>Slide 3</Text>
        </YStack>
      </SimpleCarousel>,
    );
    const indicators = screen.queryAllByTestId("carousel-indicator");
    expect(indicators.length).toBe(0);
  });

  it("should handle disabled state", () => {
    render(
      <SimpleCarousel>
        <YStack>
          <Text>Slide 1</Text>
        </YStack>
        <YStack>
          <Text>Slide 2</Text>
        </YStack>
        <YStack>
          <Text>Slide 3</Text>
        </YStack>
      </SimpleCarousel>,
    );
    act(() => {
      vi.advanceTimersByTime(1000);
    });
    expect(screen.getByText("Slide 2")).toBeInTheDocument();
  });

  it("should initialize with the default slide", () => {
    render(
      <SimpleCarousel defaultSlide={2}>
        <YStack>
          <Text>Slide 1</Text>
        </YStack>
        <YStack>
          <Text>Slide 2</Text>
        </YStack>
        <YStack>
          <Text>Slide 3</Text>
        </YStack>
      </SimpleCarousel>,
    );
    expect(screen.getByText("Slide 2")).toBeInTheDocument();
  });

  it("should not change slide if defaultSlide is out of range", () => {
    render(
      <SimpleCarousel defaultSlide={5}>
        <YStack>
          <Text>Slide 1</Text>
        </YStack>
        <YStack>
          <Text>Slide 2</Text>
        </YStack>
        <YStack>
          <Text>Slide 3</Text>
        </YStack>
      </SimpleCarousel>,
    );
    expect(screen.getByText("Slide 1")).toBeInTheDocument();
  });

  it("should handle edge cases for defaultSlide", () => {
    render(
      <SimpleCarousel defaultSlide={1}>
        <YStack>
          <Text>Slide 1</Text>
        </YStack>
        <YStack>
          <Text>Slide 2</Text>
        </YStack>
        <YStack>
          <Text>Slide 3</Text>
        </YStack>
      </SimpleCarousel>,
    );
    expect(screen.getByText("Slide 2")).toBeInTheDocument();
  });

  it("should automatically change slides based on speed", () => {
    vi.useFakeTimers();
    render(
      <SimpleCarousel speed={1000}>
        <YStack>
          <Text>Slide 1</Text>
        </YStack>
        <YStack>
          <Text>Slide 2</Text>
        </YStack>
        <YStack>
          <Text>Slide 3</Text>
        </YStack>
      </SimpleCarousel>,
    );
    expect(screen.getByText("Slide 1")).toBeInTheDocument();
    act(() => {
      vi.advanceTimersByTime(1000);
    });
    expect(screen.getByText("Slide 2")).toBeInTheDocument();
    act(() => {
      vi.advanceTimersByTime(1000);
    });
    expect(screen.getByText("Slide 3")).toBeInTheDocument();
    act(() => {
      vi.advanceTimersByTime(1000);
    });
    expect(screen.getByText("Slide 1")).toBeInTheDocument();
  });
});
