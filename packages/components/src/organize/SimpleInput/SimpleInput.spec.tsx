/**
 * File: /src/organize/SimpleInput/SimpleInput.spec.tsx
 * Project: @multiplatform.one/components
 * File Created: 31-12-2024 07:36:50
 * Author: Clay Risser
 * -----
 * BitSpur (c) Copyright 2021 - 2024
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { config } from "@tamagui/config";
import { Eye, EyeOff } from "@tamagui/lucide-icons";
import { fireEvent, render, screen, waitFor } from "@testing-library/react";
import { TamaguiProvider, createTamagui } from "tamagui";
import { SimpleInput } from "./index";

const tamaguiConfig = createTamagui(config);

describe("SimpleInput", () => {
  const renderWithProviders = (ui: React.ReactElement) => {
    return render(
      <TamaguiProvider config={tamaguiConfig} defaultTheme="light">
        {ui}
      </TamaguiProvider>,
    );
  };

  it("should render successfully", () => {
    const { getByRole } = renderWithProviders(<SimpleInput inputType="TEXT" />);
    expect(getByRole("textbox")).toBeDefined();
  });

  it("should handle text input changes", () => {
    const { getByRole, getByDisplayValue } = renderWithProviders(
      <SimpleInput inputType="TEXT" />,
    );
    const input = getByRole("textbox");
    fireEvent.change(input, { target: { value: "Hello" } });
    expect(getByDisplayValue("Hello")).toBeDefined();
  });

  it("should show error for invalid email format", () => {
    const { getByRole } = renderWithProviders(
      <SimpleInput inputType="EMAIL" />,
    );
    const input = getByRole("textbox");
    fireEvent.change(input, { target: { value: "invalid-email" } });
    expect(getByRole("textbox")).toBeDefined();
  });

  it("should accept valid email format", () => {
    const { getByRole } = renderWithProviders(
      <SimpleInput inputType="EMAIL" />,
    );
    const input = getByRole("textbox");
    fireEvent.change(input, { target: { value: "test@example.com" } });
    expect(input).toHaveValue("test@example.com");
  });

  it("should toggle password visibility", async () => {
    renderWithProviders(<SimpleInput inputType="PASSWORD" />);
    const toggleButton = screen.getByRole("button");
    const initialIcon = toggleButton.querySelector("svg");
    expect(initialIcon).toBeDefined();
    fireEvent.click(toggleButton);
    const afterClickIcon = toggleButton.querySelector("svg");
    expect(afterClickIcon).toBeDefined();
    fireEvent.click(toggleButton);
    const afterSecondClickIcon = toggleButton.querySelector("svg");
    expect(afterSecondClickIcon).toBeDefined();
  });
});
