/**
 * File: /src/organize/SimpleTabs/SimpleTabs.spec.tsx
 * Project: @multiplatform.one/components
 * File Created: 06-02-2025 05:53:11
 * Author: Clay Risser
 * -----
 * BitSpur (c) Copyright 2021 - 2025
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { fireEvent, render, screen } from "@testing-library/react";
import { Tabs } from "tamagui";
import { SimpleTabs, TabsContent, TabsList } from "./index";

describe("SimpleTabs", () => {
  it("should render successfully with horizontal orientation", () => {
    const { getByText } = render(
      <SimpleTabs orientation="horizontal" defaultValue="home">
        <TabsContent value="home">Home Content</TabsContent>
        <TabsContent value="about">About Content</TabsContent>
        <TabsContent value="contact">Contact Content</TabsContent>
      </SimpleTabs>,
    );
    expect(getByText("Home Content")).toBeInTheDocument();
  });

  it("should render successfully with vertical orientation", () => {
    const { getByText } = render(
      <SimpleTabs orientation="vertical" defaultValue="home">
        <TabsContent value="home">Home Content</TabsContent>
        <TabsContent value="about">About Content</TabsContent>
        <TabsContent value="contact">Contact Content</TabsContent>
      </SimpleTabs>,
    );
    expect(getByText("Home Content")).toBeInTheDocument();
  });

  it("should switch tabs correctly", async () => {
    render(
      <SimpleTabs orientation="horizontal" defaultValue="home">
        <TabsList>
          <Tabs.Tab value="home">Home</Tabs.Tab>
          <Tabs.Tab value="about">About</Tabs.Tab>
          <Tabs.Tab value="contact">Contact</Tabs.Tab>
        </TabsList>
        <TabsContent value="home">Home Content</TabsContent>
        <TabsContent value="about">About Content</TabsContent>
        <TabsContent value="contact">Contact Content</TabsContent>
      </SimpleTabs>,
    );
    expect(screen.getByText("Home Content")).toBeInTheDocument();
    expect(screen.queryByText("About Content")).not.toBeInTheDocument();
    fireEvent.click(screen.getByRole("tab", { name: /About/i }));
    expect(await screen.findByText("About Content")).toBeInTheDocument();
    expect(screen.queryByText("Home Content")).not.toBeInTheDocument();
  });
});
