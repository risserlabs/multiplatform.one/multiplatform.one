/**
 * File: /src/code/Pre/Pre.spec.tsx
 * Project: @multiplatform.one/components
 * File Created: 03-02-2025 10:19:22
 * Author: Vandana Madhireddy
 * -----
 * BitSpur (c) Copyright 2021 - 2025
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { config } from "@tamagui/config";
import { render } from "@testing-library/react";
import { TamaguiProvider, createTamagui } from "tamagui";
import { describe, expect, it } from "vitest";
import { Pre } from "./index";

const tamaguiConfig = createTamagui(config);

describe("Pre", () => {
  const renderWithProviders = (ui: React.ReactElement) => {
    return render(
      <TamaguiProvider config={tamaguiConfig} defaultTheme="light">
        {ui}
      </TamaguiProvider>,
    );
  };
  const defaultProps = {
    children: "Test content",
  };
  const renderPre = (props = {}) => {
    return renderWithProviders(<Pre {...defaultProps} {...props} />);
  };

  describe("Basic Rendering", () => {
    it("should render successfully", () => {
      const { container } = renderPre();
      const preElement = container.querySelector("pre");
      expect(preElement).toBeDefined();
      expect(preElement?.textContent).toBe("Test content");
    });

    it("should render with custom content", () => {
      const { container } = renderPre({
        children: "Custom content",
      });
      const preElement = container.querySelector("pre");
      expect(preElement).toBeDefined();
      expect(preElement?.textContent).toBe("Custom content");
    });
  });

  describe("Content Handling", () => {
    it("should handle empty content", () => {
      const { container } = renderPre({
        children: "",
      });
      const preElement = container.querySelector("pre");
      expect(preElement).toBeDefined();
    });

    it("should handle multiple lines", () => {
      const { container } = renderPre({
        children: `Line 1
        Line 2`,
      });
      const preElement = container.querySelector("pre");
      expect(preElement).toBeDefined();
      expect(preElement?.textContent).toContain("Line 1");
      expect(preElement?.textContent).toContain("Line 2");
    });

    it("should handle code content", () => {
      const { container } = renderPre({
        children: `
        const test = "hello";
        function greet() {
          return test;
        }`,
      });
      const preElement = container.querySelector("pre");
      expect(preElement).toBeDefined();
      expect(preElement?.textContent).toContain("const test");
    });
  });

  describe("Styling", () => {
    it("should handle custom background color", () => {
      const { container } = renderPre({
        backgroundColor: "$blue10",
      });
      const preElement = container.querySelector("pre");
      expect(preElement).toBeDefined();
    });

    it("should handle custom text color", () => {
      const { container } = renderPre({
        color: "$color12",
      });
      const preElement = container.querySelector("pre");
      expect(preElement).toBeDefined();
    });
  });

  describe("Dynamic Updates", () => {
    it("should update content when changed", () => {
      const { container, rerender } = renderPre();
      rerender(
        <TamaguiProvider config={tamaguiConfig} defaultTheme="light">
          <Pre>Updated content</Pre>
        </TamaguiProvider>,
      );
      const preElement = container.querySelector("pre");
      expect(preElement?.textContent).toBe("Updated content");
    });
  });

  describe("Edge Cases", () => {
    it("should handle special characters", () => {
      const { container } = renderPre({
        children: "Special chars: @#$%^&*",
      });
      const preElement = container.querySelector("pre");
      expect(preElement).toBeDefined();
      expect(preElement?.textContent).toContain("Special chars: @#$%^&*");
    });

    it("should handle very long content", () => {
      const { container } = renderPre({
        children: "x".repeat(1000),
      });
      const preElement = container.querySelector("pre");
      expect(preElement).toBeDefined();
      expect(preElement?.textContent?.length).toBe(1000);
    });
  });

  describe("Accessibility", () => {
    it("should have proper ARIA attributes", () => {
      const { container } = renderPre({
        "aria-label": "Code block",
      });
      const preElement = container.querySelector("pre");
      expect(preElement).toBeDefined();
      expect(preElement?.getAttribute("aria-label")).toBe("Code block");
    });
  });
});
