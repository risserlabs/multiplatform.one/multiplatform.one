/**
 * File: /src/code/Code/Code.spec.tsx
 * Project: @multiplatform.one/components
 * File Created: 30-01-2024 10:03:48
 * Author: Clay Risser
 * -----
 * BitSpur (c) Copyright 2021 - 2024
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { render } from "@testing-library/react";
import { describe, expect, it } from "vitest";
import { Code, CodeInline } from "./index";

describe("Code", () => {
  it("should render successfully", () => {
    const { container } = render(<Code>const test = "hello";</Code>);
    const codeElement = container.querySelector("code");
    expect(codeElement).toBeDefined();
    expect(codeElement?.textContent).toBe('const test = "hello";');
  });

  it("should handle colored variant", () => {
    const { container } = render(<Code colored>const test = "hello"</Code>);
    const codeElement = container.querySelector("code");
    expect(codeElement).toBeDefined();
  });

  it("should apply correct styles", () => {
    const { container } = render(<Code>const test = "hello"</Code>);
    const codeElement = container.querySelector("code");
    expect(codeElement).toBeDefined();
  });
});

describe("CodeInline", () => {
  it("should render successfully", () => {
    const { container } = render(<CodeInline>npm install</CodeInline>);
    const codeElement = container.querySelector("code");
    expect(codeElement).toBeDefined();
    expect(codeElement?.textContent).toBe("npm install");
  });

  it("should apply correct styles", () => {
    const { container } = render(<CodeInline>npm install</CodeInline>);
    const codeElement = container.querySelector("code");
    expect(codeElement).toBeDefined();
  });

  it("should handle custom styling", () => {
    const { container } = render(<CodeInline>npm install</CodeInline>);
    const codeElement = container.querySelector("code");
    expect(codeElement).toBeDefined();
  });
});
