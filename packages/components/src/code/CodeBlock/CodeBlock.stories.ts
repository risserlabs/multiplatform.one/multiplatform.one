/*
 * File: /src/code/CodeBlock/CodeBlock.stories.ts
 * Project: @multiplatform.one/components
 * File Created: 01-02-2025 04:11:48
 * Author: FFX User
 * -----
 * BitSpur (c) Copyright 2021 - 2025
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import type { Meta, StoryObj } from "@storybook/react";
import { CodeBlock } from "./index";

const meta: Meta = {
  title: "code/CodeBlock",
  component: CodeBlock,
  parameters: {
    status: { type: "beta" },
  },
};

export const main: StoryObj<typeof CodeBlock> = {
  args: {
    backgroundColor: "$color2",
    debug: false,
    disableCopy: false,
    isCollapsible: false,
    isHighlightingLines: true,
    showLineNumbers: false,
    children: `
const hello = 'world';

function howdy() {
  return 'texas';
}
`,
  },
};

export default meta;
