/**
 * File: /src/code/CodeBlock/CodeBlock.spec.tsx
 * Project: @multiplatform.one/components
 * File Created: 31-01-2025 05:24:17
 * Author: ffx
 * -----
 * BitSpur (c) Copyright 2021 - 2025
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { render } from "@testing-library/react";
import { describe, expect, it, vi } from "vitest";
import { Code, Pre } from "../index";
import { CodeBlock } from "./index";

vi.mock("../../mdx/MDX", () => ({
  MDX: ({ children }: { children: React.ReactNode }) => (
    <Pre>
      <Code>{children}</Code>
    </Pre>
  ),
}));

vi.mock("../../mdx/MDXCodeBlock/MDXCodeBlockContext", () => ({
  MDXCodeBlockContext: {
    Provider: ({ children }: { children: React.ReactNode }) => children,
  },
}));

describe("CodeBlock", () => {
  it("should render successfully", () => {
    const { container } = render(<CodeBlock>const test = "hello";</CodeBlock>);
    const codeElement = container.querySelector("code");
    expect(codeElement).toBeDefined();
    expect(codeElement?.textContent).toContain('const test = "hello"');
  });

  it("should handle language prop", () => {
    const { container } = render(
      <CodeBlock language="typescript">
        const greeting: string = "hello"
      </CodeBlock>,
    );
    const codeElement = container.querySelector("code");
    expect(codeElement).toBeDefined();
    expect(codeElement?.textContent).toContain(
      'const greeting: string = "hello"',
    );
  });

  it("should handle debug mode", () => {
    const { container } = render(
      <CodeBlock debug>const test = true;</CodeBlock>,
    );
    const codeElement = container.querySelector("code");
    expect(codeElement).toBeDefined();
  });

  describe("dynamic updates", () => {
    it("should update content when changed", () => {
      const { container, rerender } = render(
        <CodeBlock>const initial = "hello";</CodeBlock>,
      );
      rerender(<CodeBlock>const updated = "world";</CodeBlock>);
      const codeElement = container.querySelector("code");
      expect(codeElement?.textContent).toBe(
        '```tsx\nconst updated = "world";\n```',
      );
    });

    it("should update language when changed", () => {
      const { container, rerender } = render(
        <CodeBlock language="javascript">const test = true;</CodeBlock>,
      );
      rerender(<CodeBlock language="python">const test = true;</CodeBlock>);
      const codeElement = container.querySelector("code");
      expect(codeElement?.textContent).toContain("```python");
    });
  });

  describe("edge cases", () => {
    it("should handle empty content", () => {
      const { container } = render(<CodeBlock />);
      const codeElement = container.querySelector("code");
      expect(codeElement).toBeDefined();
    });

    it("should handle whitespace", () => {
      const { container } = render(
        <CodeBlock>
          {`
            const test = true;
          `}
        </CodeBlock>,
      );
      const codeElement = container.querySelector("code");
      expect(codeElement?.textContent).toContain("const test = true");
    });
  });
});
