/**
 * File: /src/images/images.spec.tsx
 * Project: @multiplatform.one/components
 * File Created: 03-02-2025 17:38:39
 * Author: Bit Spur
 * -----
 * BitSpur (c) Copyright 2021 - 2025
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { render } from "@testing-library/react";
import { SimpleImage } from "./SimpleImage/index";
import { Svg } from "./Svg/index";
import { Svg as NativeSvg } from "./Svg/index.native";
import { SvgUri } from "./SvgUri/index";
import * as Images from "./index";

describe("Images Module", () => {
  it("should export SimpleImage", () => {
    expect(SimpleImage).toBeDefined();
  });
  it("should render SimpleImage correctly", () => {
    const { container } = render(<SimpleImage source={{ uri: "test-uri" }} />);
    expect(container).toBeDefined();
  });
  it("should export Svg", () => {
    expect(Svg).toBeDefined();
  });
  it("should render Svg correctly", () => {
    const { container } = render(<Svg />);
    expect(container).toBeDefined();
  });
  it("should export SvgUri", () => {
    expect(SvgUri).toBeDefined();
  });
  it("should render SvgUri correctly", () => {
    const { container } = render(<SvgUri uri="test-uri" />);
    expect(container).toBeDefined();
  });
  it("should export all components from index", () => {
    expect(Images.SimpleImage).toBeDefined();
    expect(Images.Svg).toBeDefined();
    expect(Images.SvgUri).toBeDefined();
  });
  it("should render SimpleImage from index correctly", () => {
    const { container } = render(
      <Images.SimpleImage source={{ uri: "test-uri" }} />,
    );
    expect(container).toBeDefined();
  });
  it("should render Svg from index correctly", () => {
    const { container } = render(<Images.Svg />);
    expect(container).toBeDefined();
  });
  it("should render SvgUri from index correctly", () => {
    const { container } = render(<Images.SvgUri uri="test-uri" />);
    expect(container).toBeDefined();
  });
  it("renders correctly", () => {
    const { getByTestId } = render(<NativeSvg testID="svg-component" />);
    const svgComponent = getByTestId("svg-component");
    expect(svgComponent).toBeTruthy();
  });
});
