/**
 * File: /src/images/Svg/Svg.spec.tsx
 * Project: @multiplatform.one/components
 * File Created: 31-01-2025 09:41:25
 *
 * -----
 * BitSpur (c) Copyright 2021 - 2025
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { act, render } from "@testing-library/react";
import { Circle } from "react-native-svg";
import { Svg } from "./index";

describe("Svg Component", () => {
  it("renders without crashing", () => {
    const { container } = render(
      <Svg width="100" height="100">
        <Circle cx="50" cy="50" r="40" stroke="black" fill="none" />
      </Svg>,
    );
    expect(container).toBeTruthy();
  });
  it("accepts and applies custom dimensions", () => {
    const { getByTestId } = render(
      <Svg width={200} height={150} data-testid="custom-svg">
        <Circle cx="50" cy="50" r="40" stroke="black" fill="none" />
      </Svg>,
    );
    const svgElement = getByTestId("custom-svg");
    expect(svgElement.style.width).toBe("200px");
    expect(svgElement.style.height).toBe("150px");
  });
  it("accepts and applies custom styles", () => {
    const { container } = render(
      <Svg
        data-testid="styled-svg"
        width="100"
        height="100"
        style={{ opacity: 0.5 }}
      >
        <Circle cx="50" cy="50" r="40" stroke="black" fill="none" />
      </Svg>,
    );
    const svgElement = container.querySelector("svg");
    expect(svgElement).toBeTruthy();
    expect(svgElement?.style.opacity).toBe("0.5");
  });
  it("renders children correctly", () => {
    const { getByTestId } = render(
      <Svg width="100" height="100">
        <Circle
          data-testid="circle-element"
          cx="50"
          cy="50"
          r="40"
          stroke="black"
          fill="none"
        />
      </Svg>,
    );
    expect(getByTestId("circle-element")).toBeTruthy();
  });

  describe("dynamic behavior", () => {
    it("updates dimensions when props change", () => {
      const { getByTestId, rerender } = render(
        <Svg width={100} height={100} data-testid="dynamic-svg">
          <Circle cx="50" cy="50" r="40" stroke="black" fill="none" />
        </Svg>,
      );
      const svgElement = getByTestId("dynamic-svg");
      expect(svgElement.style.width).toBe("100px");
      expect(svgElement.style.height).toBe("100px");
      rerender(
        <Svg width={200} height={150} data-testid="dynamic-svg">
          <Circle cx="50" cy="50" r="40" stroke="black" fill="none" />
        </Svg>,
      );
      expect(svgElement.style.width).toBe("200px");
      expect(svgElement.style.height).toBe("150px");
    });
    it("updates styles dynamically", () => {
      const { getByTestId, rerender } = render(
        <Svg
          data-testid="dynamic-style-svg"
          width={100}
          height={100}
          style={{ opacity: 0.5 }}
        >
          <Circle cx="50" cy="50" r="40" stroke="black" fill="none" />
        </Svg>,
      );
      const svgElement = getByTestId("dynamic-style-svg");
      expect(svgElement.style.opacity).toBe("0.5");
      rerender(
        <Svg
          data-testid="dynamic-style-svg"
          width={100}
          height={100}
          style={{ opacity: 0.8 }}
        >
          <Circle cx="50" cy="50" r="40" stroke="black" fill="none" />
        </Svg>,
      );
      expect(svgElement.style.opacity).toBe("0.8");
    });
    it("updates children dynamically", () => {
      const { getByTestId, rerender } = render(
        <Svg width={100} height={100}>
          <Circle
            data-testid="dynamic-circle"
            cx="50"
            cy="50"
            r="40"
            stroke="black"
            fill="none"
          />
        </Svg>,
      );
      const circleElement = getByTestId("dynamic-circle");
      expect(circleElement.getAttribute("r")).toBe("40");
      rerender(
        <Svg width={100} height={100}>
          <Circle
            data-testid="dynamic-circle"
            cx="50"
            cy="50"
            r="60"
            stroke="black"
            fill="none"
          />
        </Svg>,
      );
      expect(circleElement.getAttribute("r")).toBe("60");
    });
  });
});
