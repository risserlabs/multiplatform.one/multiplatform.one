/**
 * File: /src/router/reactRouter.tsx
 * Project: @multiplatform.one/router
 * File Created: 24-01-2025 16:56:40
 * Author: Clay Risser
 * -----
 * BitSpur (c) Copyright 2021 - 2025
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {
  Link as ReactRouterLink,
  useLocation,
  useNavigate,
  useParams as useReactParams,
} from "react-router-dom";
import type {
  Href,
  InputRouteParams,
  InputRouteParamsBlank,
  LinkComponent,
  Router,
} from "../types";

export const useRouter = (): Router => {
  const location = useLocation();
  const navigate = useNavigate();
  return {
    back: () => navigate(-1),
    canGoBack: () => true,
    push: (href: Href) => navigate(href as string, { replace: false }),
    navigate: (href: Href) => navigate(href as string, { replace: false }),
    replace: (href: Href) => navigate(href as string, { replace: true }),
    dismiss: (count?: number) => navigate(-1 * (count || 1)),
    dismissAll: () => navigate("/"),
    canDismiss: () => true,
    setParams: <T extends string = "">(
      params?: T extends "" ? InputRouteParamsBlank : InputRouteParams<T>,
    ) => {
      const searchParams = new URLSearchParams(location.search);
      if (params) {
        Object.entries(params).forEach(([key, value]) => {
          if (value === undefined || value === null) {
            searchParams.delete(key);
          } else {
            searchParams.set(key, String(value));
          }
        });
      }
      navigate({ search: searchParams.toString() });
    },
    subscribe: (listener) => {
      listener("success");
      return () => {};
    },
    onLoadState: (listener) => {
      listener("loaded");
      return () => {};
    },
  };
};

export const usePathname = (): string => useLocation().pathname;

export const useParams = useReactParams;

export const Link: LinkComponent = ({ href, replace, children, ...props }) => (
  <ReactRouterLink to={href as string} replace={replace} {...props}>
    {children}
  </ReactRouterLink>
);

Link.resolveHref = (href: Href) => href.toString();
