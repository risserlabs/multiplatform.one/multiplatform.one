#!/bin/sh

set -e

_get_remote_name() {
    _BASE_NAME="multiplatform"
    _NUMBER="$(date +%s)"
    _REMOTE_NAME="$_BASE_NAME.$_NUMBER"
    while git remote | grep -q "^$_REMOTE_NAME$"; do
        _NUMBER="$((_NUMBER + 1))"
        _REMOTE_NAME="$_BASE_NAME.$_NUMBER"
    done
    echo "$_REMOTE_NAME"
}

_PROJECT_DIR="$(pwd)"
_STAGING_DIR="$(mktemp -d)"
trap "rm -rf \"$_STAGING_DIR\"" EXIT
cd "$_STAGING_DIR"
$COOKIECUTTER "$@"
cd "$_PROJECT_DIR"
git clean -fxd
_COOKIECUTTER_DIR="$_STAGING_DIR/$(ls "$_STAGING_DIR")"
_INITIAL_COMMIT="$(git rev-parse HEAD)"
_REMOTE_NAME="$(_get_remote_name)"
git remote add "$_REMOTE_NAME" "$_COOKIECUTTER_DIR"
git fetch "$_REMOTE_NAME"
git merge --allow-unrelated-histories "$_REMOTE_NAME/main" || true
git remote remove "$_REMOTE_NAME"
if [ -f .updateignore ]; then
    while IFS= read -r pattern; do
        find . -name "$pattern" -exec git checkout "$_INITIAL_COMMIT" -- '{}' +
    done < .updateignore
fi
while IFS= read -r pattern; do
    find . -name "$pattern" -exec git checkout "$_INITIAL_COMMIT" -- '{}' +
done <<EOF
.mkpm/cache.tar.gz
platforms/storybook/.lostpixel/baseline
pnpm-lock.yaml
EOF
./mkpm check
