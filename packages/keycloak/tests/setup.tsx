/**
 * File: /tests/setup.tsx
 * Project: @multiplatform.one/components
 * File Created: 17-01-2025 18:50:03
 * Author: Clay Risser
 * -----
 * BitSpur (c) Copyright 2021 - 2025
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import "@testing-library/jest-dom";
import "matchmedia-polyfill";
import { config } from "@tamagui/config";
import { cleanup } from "@testing-library/react";
import { createTamagui } from "tamagui";
import { afterEach } from "vitest";

createTamagui(config);

window.matchMedia =
  window.matchMedia ||
  (() => ({
    matches: false,
    addListener: () => {},
    removeListener: () => {},
  }));

global.requestAnimationFrame = ((callback: FrameRequestCallback): number => {
  return setTimeout(callback, 0) as unknown as number;
}) as typeof window.requestAnimationFrame;

global.cancelAnimationFrame = (id: number): void => {
  clearTimeout(id);
};

class ResizeObserver {
  observe() {}
  unobserve() {}
  disconnect() {}
}

window.ResizeObserver = ResizeObserver;

class MockIntersectionObserver implements IntersectionObserver {
  readonly root: Element | null = null;
  readonly rootMargin: string = "0px";
  readonly thresholds: ReadonlyArray<number> = [0];

  observe(): void {}
  unobserve(): void {}
  disconnect(): void {}
  takeRecords(): IntersectionObserverEntry[] {
    return [];
  }
}

window.IntersectionObserver = MockIntersectionObserver;

class ESBuildAndJSDOMCompatibleTextEncoder extends TextEncoder {
  encode(input: string) {
    if (typeof input !== "string")
      throw new TypeError("`input` must be a string");
    const decodedURI = decodeURIComponent(encodeURIComponent(input));
    const arr = new Uint8Array(decodedURI.length);
    const chars = decodedURI.split("");
    for (let i = 0; i < chars.length; i++) {
      arr[i] = decodedURI[i].charCodeAt(0);
    }
    return arr;
  }
}

globalThis.TextEncoder = ESBuildAndJSDOMCompatibleTextEncoder;

afterEach(() => {
  cleanup();
});
