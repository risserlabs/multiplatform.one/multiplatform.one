/*
 * File: /src/keycloak/index.electron.ts
 * Project: @multiplatform.one/keycloak
 * File Created: 10-01-2025 21:02:36
 * Author: Clay Risser
 * -----
 * BitSpur (c) Copyright 2021 - 2025
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import type KeycloakClient from "keycloak-js";
import { useContext } from "react";
import { useAuthConfig } from "../hooks";
import type { KeycloakMock } from "../types";
import {
  BaseKeycloak,
  type KeycloakLoginOptions,
  type KeycloakLogoutOptions,
} from "./base";
import { KeycloakConfigContext } from "./config";
import { KeycloakContext } from "./context";

export class Keycloak extends BaseKeycloak {
  protected _handleInputObject(input: KeycloakClient | KeycloakMock) {
    if (typeof (input as KeycloakClient).init === "function") {
      this._keycloakClient = input as KeycloakClient;
    } else {
      this._mock = input as KeycloakMock;
    }
  }

  async login(options: KeycloakLoginOptions = {}) {
    this._clear();
    if (this._keycloakClient) {
      await this._keycloakClient.login(options);
    } else {
      await this._login?.(options);
    }
    this._sync();
  }

  async logout(options: KeycloakLogoutOptions = {}) {
    if (this._keycloakClient) {
      await this._keycloakClient.logout(options);
    } else {
      await this._logout?.(options);
    }
    this._clear();
  }
}

export function useKeycloak() {
  const keycloak = useContext(KeycloakContext);
  const keycloakConfig = useContext(KeycloakConfigContext);
  const { disabled } = useAuthConfig();
  if (disabled) return null;
  if (keycloak) return keycloak;
}

export * from "./base";
export * from "./config";
